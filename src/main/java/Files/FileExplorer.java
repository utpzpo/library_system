package Files;


import com.library.sql.SQLController;
import java.awt.Desktop;
import java.io.File;
import java.net.URL;
import javafx.application.Application;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 
 * @author kucht
 */
public class FileExplorer extends Application{
    
    private Desktop desktop = Desktop.getDesktop();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Eksplorator plików");
        
        final FileChooser fileChooser = new FileChooser();
        URL path = FileExplorer.class.getResource("FileExplorer.class");
        fileChooser.setInitialDirectory(new File(SQLController.getInstance().URL));
        fileChooser.getExtensionFilters().addAll(
            new FileChooser.ExtensionFilter("All","*.*"));
        fileChooser.showOpenDialog(primaryStage);
        primaryStage.show();
    }
    
    


    
}
