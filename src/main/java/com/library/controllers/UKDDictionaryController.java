/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import static com.library.controllers.MainController.logger;
import com.library.sql.SQLController;
import java.io.IOException;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.InputMethodEvent;
import javafx.stage.Stage;

/**
 *
 * @author kucht
 */
public class UKDDictionaryController extends AbstractDictionaryController {
    @FXML
    void onCloseActionButton(ActionEvent event) {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }
    
    @FXML
    void onInputMethodTextChanged(InputMethodEvent event) {
        
    }

    @FXML
    void onSaveActionButton(ActionEvent event) {
        SQLController.getInstance().saveDictionary((ArrayList) table.getItems(), "UKDDictionary");
    }

    @FXML
    void initialize() {
        save.setDisable(true);
        label.setText("UKD");
    }
    public UKDDictionaryController() throws IOException
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/Dictionary.fxml"));
        loader.setController(this);
        Parent root= (Parent) loader.load();
        Stage stage = new Stage();
        stage.setTitle("Słownik UKD");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
        logger.info("Wywołanie okna "+this.getClass().toString());        
    }    
}
