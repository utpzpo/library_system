/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.BooksItem;
import com.library.entities.Reader;
import com.library.sql.SQLController;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 *
 * @author kucht
 */
public class HistoryController {
    public static int ID=0;
    /**
     * Label, w którym jest wyszczególniony, kogo dotyczy historia.
     */
    @FXML
    private Label label;
    /**
     * Tabelka, w której są wyszczególnione wypożyczone książki
     * przez danego czytelnika.
     */
    @FXML
    private TableView<BooksItem> historyTable;
    /**
     * Przycisk zamykający okno.
     */
    @FXML
    private Button close;
    /**
     * Funkcja zamykająca okno. Wywoływana przez wciśnięcie przycisku close.
     * @param event - przyciśnięcie przycisku close
     */
    @FXML
    void onCloseActionButton(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }
   
    @FXML
    public void initialize()
    {
        TableColumn<BooksItem, Integer> id = new TableColumn<>("ID książki");
        id.setCellValueFactory(cellData -> cellData.getValue().bookIdProperty().asObject());
        TableColumn<BooksItem, String> title = new TableColumn<>("Tytuł");
        title.setCellValueFactory(cellData -> cellData.getValue().bookTitleProperty());
        TableColumn<BooksItem, String> date = new TableColumn<>("Data zwrotu");
        title.setCellValueFactory(cellData -> cellData.getValue().bookDateProperty());
        historyTable.getColumns().addAll(id,title,date);
        historyTable.setItems(FXCollections.observableArrayList(
                SQLController.getInstance().getAllRentedBooksByReader(ID)));
        historyTable.setEditable(false);
        Reader r = SQLController.getInstance().getReader(ID);
        label.setText(label.getText()+" "+r.getName()+" "+r.getSurname());
    }
}
