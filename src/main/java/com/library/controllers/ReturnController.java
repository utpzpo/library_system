/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.BookBorrow;
import com.library.sql.SQLController;
import java.time.LocalDate;
import java.util.Date;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kucht
 */
public class ReturnController {
    public static Logger logger = LoggerFactory.getLogger(MainController.class);
       /**
     * fx:id="ZBookID"
     * Pole tekstowe, w którym należy wpisać numer inwentarzowy dla książki.
     * Domyślnie wypełniany
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZBookID;
    /**
     * fx:id="ZISBN"
     * Pole tekstowe, w którym należy wpisać numer ISBN książki.
     * Domyślnie wypełniany
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZISBN; 
    /**
     * fx:id="ZAuthor"
     * Pole tekstowe, w którym znajduje się nazwa autora zwracanej książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZAuthor;
    /**
     * fx:id="ZTitle"
     * Pole tekstowe, w którym znajduje się tytuł zwracanej książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField ZTitle; 
    /**
     * fx:id="ZYear"
     * Pole tekstowe, w którym znajduje się rok wydania zwracanej książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField ZYear;
    /**
     * fx:id="ZCondition"
     * Pole tekstowe, w którym znajduje się stan zwracanej książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZCondition;
    /**
     * fx:id="ZKind"
     * Pole tekstowe, w którym znajduje się kategoria zwracanej książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField ZKind;
    /**
     * fx:id="ZReaderID"
     * Pole tekstowe, w którym należy wpisać nr czytelnika.
     * Może zostać przekazany z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZReaderID;
    /**
     * fx:id="ZSurname"
     * Pole tekstowe, w którym znajduje się nazwisko czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField ZSurname; 
    /**
     * fx:id="ZName
     * Pole tekstowe w którym znajduje się imię czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZName;
    /**
     * fx:id="ZLoans"
     * Przycisk, który wywołuje okno z aktualną tabelą wypożyczeń książek,
     * które czytelnik ma na swoim stanie.
     * Value injected by FXMLLoader
     */
    @FXML
    Button ZLoans;
    /**
     * fx:id="ZLoanDate"
     * Pole tekstowe, w którym podstawiana jest data wypożyczenia książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField ZLoanDate;
    /**
     * fx:id="ZToday"
     * Pole tekstowe, w którym podstawiana jest aktualna data.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField ZToday;
    /**
     * fx:id="ZOverdue"
     * Pole tekstowe, w którym stwierdzane jest, czy zwrot nastąpił po terminie.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField ZOverdue;
    /**
     * fx:id="ZReturnBook"
     * Przycisk, który powoduje zwrot książki.
     * W przypadku, gdy zwrot nastąpił po terminie, otwiera nowe okno z 
     * naliczeniem kwoty należnej do zapłaty.
     * Value injected by FXMLLoader
     */
    @FXML 
    Button ZReturnBook; 
    
    @FXML
    void onZLoansActionButton(ActionEvent event) {

    }

    @FXML
    void onZReturnBookActionButton(ActionEvent event) {

    }
    @FXML
    void initialize()
    {
        ZToday.setText(LocalDate.now().toString());
    }
    public void setReader(String id, String name, String surname)
    {
        ZReaderID.setText(id);
        ZName.setText(name);
        ZName.setText(surname);
    }
    
    public void setBook(BookBorrow bb)
    {
        ZBookID.setText(String.valueOf(bb.getID()));
        ZISBN.setText(bb.getISBN());
        ZAuthor.setText(bb.getAuthor());
        ZTitle.setText(bb.getTitle());
        ZYear.setText(bb.getYear());
        ZLoanDate.setText(SQLController.getInstance().getBorrowedBookDate(bb.getID()));
    }
}
