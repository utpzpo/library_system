/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

/**
 *
 * @author kucht
 */
public enum Status {
    
    /**
     *
     */
    DOSTEPNY("DOSTĘPNY"),NIEDOSTEPNY("NIEDOSTĘPNY");
    private final String status;
    
    Status(String status)
    {
        this.status=status;
    }
    
    public String status()
    {
        return status;
    }
    
    
}
