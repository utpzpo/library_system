/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.Reader;
import com.library.entities.ReaderBorrow;
import com.library.sql.SQLController;
import java.io.IOException;
import java.util.List;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kucht
 */
public class ReadersController {
    public static Logger logger = LoggerFactory.getLogger(MainController.class);

    @FXML
    private BorrowController borrowController;
    public void setBorrowController(BorrowController borrowController)
    {
        this.borrowController=borrowController;
    }
    @FXML 
    ReturnController returnController;
    public void setReturnController(ReturnController returnController)
    {
        this.returnController=returnController;
    }
    @FXML
    ComunicationController comunicationController;
    public void setComunicationController(ComunicationController comunicationController)
    {
        this.comunicationController=comunicationController;
    }
    /**
     * fx:id="CIDReader"
     * Pole tekstowe wyszukiwarki, w które należy wpisać nr czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField CIDReader;
    /**
     * fx:id="CPESEL"
     * Pole tekstowe wyszukiwarki, w które należy wpisać nr PESEL.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField CPESEL;
    /**
     * fx:id="CName"
     * Pole tekstowe wyszukiwarki, w które należy wpisać imię czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField CName;
    /**
     * fx:id="CSurname"
     * Pole tekstowe wyszukiwarki, w które należy wpisać nazwisko czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField CSurname;
    /**
     * fx:id="CDocumentID"
     * Pole tekstowe wyszukiwarki, w które należy wpisać nr dokumentu czytelnika,
     * wprowadzonego do bazy.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField CDocumentID;
    /**
     * fx:id="CSearch"
     * Przycisk wywołujący szukanie czytelnika w bazie danych.
     * @see onCSearchActionButton
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CSearch;
    /**
     * fx:id="CClear"
     * Przycisk wywołujący czyszczenie wszystkich pól wyszukiwarki.
     * @see onCClearActionButton
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CClear;
    /**
     * fx:id="CTable"
     * Tabela wyświetlająca dane wszystkich czytelników.
     * W przypadku, gdy zostanie użyta wyszukiwarka, widok jest odświeżany.
     * Value injected by FXMLLoader
     */
    @FXML
    public TableView<Reader> CTable;
    /**
     * fx:id="CAdd"
     * Przycisk wywołujący nowe okno z możliwością dodania nowego czytelnika
     * do bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CAdd;
    /**
     * fx:id="CEdit"
     * Przycisk wywołujący nowe okno z możliwością edycji aktualnie zaznaczonej
     * książki.
     * W przypadku braku zaznaczenia książki, wyświetla odpowiedni komunikat.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CEdit;
    /**
     * fx:id="CReturn"
     * Przycisk przekazujący dane zaznaczonej książki do zakładki Zwroty.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CReturn;
    /**
     * fx:id="CRent"
     * Przycisk przekazujący dane zaznaczonej ksiązki do zakładki Wypożyczenia.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CRent;
    /**
     * fx:id="CHistory"
     * Przycisk otwierający nowe okno z historią wypożyczeń dla zaznaczonego
     * czytelnika.
     * W przypadku braku zaznaczenia, wyświetla odpowiedni komunikat.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CHistory;
    /**
     * fx:id="CCommunicate"
     * Przycisk przekazujący dane zaznaczonego czytelnika do zakładki Komunikaty.
     * W przypadku braku zaznaczenia, wyświetla odpowiedni komunikat.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CCommunicate;
    /**
     * Panel, w którym znajdują się wszystkie pola do dodania czytelnika.
     */
    @FXML
    private GridPane CFieldsPane;
     /**
     * Obserwator listy użytkowników.
     * @see ObservableList
     */
    private ObservableList<Reader> readersList = FXCollections.observableArrayList();
//    /**
//     * Właściwość dla listy z userami.
//     * @see ListProperty
//     */
//    private ListProperty<Reader> readerProperty;
    private List<Reader> readers;
    
       /**
     * Otwiera nowe okno dodawania czytelnika.
     * @param event 
     */
    @FXML
    void onCAddActionButton(ActionEvent event) {
        try {
            AddReaderController addReaderController = new AddReaderController();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        BooleanProperty bp = new SimpleBooleanProperty();
        bp.set(AddReaderController.isAdded);
        bp.addListener((observable, oldValue, newValue) -> {
            readers=SQLController.getInstance().getAllReaders();
            readersList.clear();
            readersList.addAll(readers);
        });

    }
    /**
     * Czyści wszystkie pola w zakładce czytelnik.
     * @param event 
     */
    @FXML
    void onCClearActionButton(ActionEvent event) {
        CFieldsPane.getChildren().stream()
                .filter(
                        (node) -> (node instanceof TextField))
                .forEach((node) -> { ((TextField) node).clear();
        });
    }
    /**
     * Przycisk który przenosi część danych do zakładki komunikaty.
     * Wywołuje wyszukiwanie zaznaczonego czytelnika w bazie,
     * uzupełnia jego dane w zakładce Komunikaty.
     * @param event - wywołuje event
     */
    @FXML
    void onCComunicateActionButton(ActionEvent event) {
            ReaderBorrow rb = SQLController.getInstance().getReaderToBorrow(
                                CTable.getSelectionModel().getSelectedItem().getId());
            comunicationController.setReader(rb);
    }
    /**
     * Wywołuje okno, w którym można edytować dane czytelnika.
     * @param event 
     */
    @FXML
    void onCEditActionButton(ActionEvent event) throws IOException {
        if(CTable.getSelectionModel().getSelectedItem()==null)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd !!!");
            alert.setHeaderText("Nie możesz edytować użytkownika, jeśli żadnego"
                    + "nie zaznaczyłeś w tabeli!!");
            alert.showAndWait();
        }
        else
        {     
            try
            {
                EditReaderController edc = new EditReaderController();
            } catch (IOException ex) {
            logger.error(ex.getMessage());
            }
        }
        BooleanProperty bp = new SimpleBooleanProperty();
        bp.set(EditReaderController.isAdded);
        bp.addListener((observable, oldValue, newValue) -> {
            readers=SQLController.getInstance().getAllReaders();
            readersList.clear();
            readersList.addAll(readers);
        });
    }
    /**
     * Wywołuje nowe okno, w którym można przejrzeć kartę biblioteczną
     * zaznaczonego czytelnika.
     * Jeśli czytelnik nie jest zaznaczony w tabeli, wywołuje odpowiedni alert.
     * @param event 
     */
    @FXML
    void onCHistoryActionButton(ActionEvent event) throws IOException {
        Parent root= FXMLLoader.load(getClass().getResource("/fxml/History.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Historia wypożyczeń");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
        logger.info(event.getEventType().toString()+ " Wywołanie okna ");
        HistoryController.ID=CTable.getSelectionModel().getSelectedItem().getId();
    }

    @FXML
    void onCRentActionButton(ActionEvent event) throws IOException {
        if(CTable.getSelectionModel().getSelectedItem()==null)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd !!!");
            alert.setHeaderText("Nie możesz przekazać użytkownika, jeśli żadnego"
                    + "nie zaznaczyłeś w tabeli!!");
            alert.showAndWait();
        }
        else
        {
            ReaderBorrow rb;
            int id=CTable.getSelectionModel().getSelectedItem().getId();
            
            rb = SQLController.getInstance().getReaderToBorrow(id);
            borrowController.setReader(rb);
        }
    }

    @FXML
    void onCReturnActionButton(ActionEvent event) {
        //sprawdzenie, czy wiersz w tabeli jest zaznaczony
        if(CTable.getSelectionModel().getSelectedItem()==null)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Błąd !!!");
            alert.setHeaderText("Nie możesz edytować użytkownika, jeśli żadnego"
                    + "nie zaznaczyłeś w tabeli!!");
            alert.showAndWait();
        }
        else
        {
            ReaderBorrow rb = SQLController.getInstance().getReaderToBorrow(
                                CTable.getSelectionModel().getSelectedItem().getId());
            returnController.setReader(String.valueOf(rb.getId()), rb.getName(), rb.getSurname());
        }
    }

    @FXML
    void onCSearchActionButton(ActionEvent event) {
        //trik polega na tym, ze jeśli pole jest puste wstawiane
        //jest wartość null, w ten sposób nie trzeba ustawiać
        //wielu różnych zapytań
        readers=SQLController.getInstance().getReaderWithSearchDetails(
                CIDReader.getText().length()>0?Integer.valueOf(CIDReader.getText()):0,
                CPESEL.getText().length()>0?CPESEL.getText():"NULL",
                CName.getText().length()>0?CName.getText():"NULL",
                CSurname.getText().length()>0?CSurname.getText():"NULL");
        readersList.clear();
        readersList.addAll(readers);
        
    }

    @FXML
    void onCTableSortAction(ActionEvent event) {

    }
    @FXML
    public void initialize()
    {
        readers = SQLController.getInstance().getAllReaders();
        readersList.addAll(readers);
        //Tworzenie kolumn dla tabeli
        TableColumn<Reader, Integer> id = new TableColumn<>("ID");
        id.setCellValueFactory(cellData -> 
                cellData.getValue().readerIdProperty().asObject());
        TableColumn<Reader, String> name = new TableColumn<>("Imię");
        name.setCellValueFactory(cellData ->
                cellData.getValue().readerNameProperty());
        TableColumn<Reader, String> surname = new TableColumn<>("Nazwisko");
        surname.setCellValueFactory(cellData ->
                cellData.getValue().readerSurnameProperty());
        TableColumn<Reader, String> pesel = new TableColumn<>("PESEL");
        pesel.setCellValueFactory(cellData ->
                cellData.getValue().readerPESELProperty());        
        TableColumn<Reader, String> number = new TableColumn<>("Ilość wypożyczonych książek");
        number.setCellValueFactory(cellData ->
                cellData.getValue().readerNumberOfRentedBooks());
        CTable.setEditable(false);
        CTable.setItems(readersList);
        CTable.getColumns().addAll(id,name,surname,pesel,number);
        CTable.setOnMouseClicked((MouseEvent event ) ->{
            if(event.getButton().equals(MouseButton.PRIMARY))
            {
                if(CTable.getSelectionModel().getSelectedItem()!=null)
                {
                    EditReaderController.readerID = CTable.getSelectionModel().getSelectedItem().getId();
                }
            }
        });
    }

}
