/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

/**
 *
 * @author kucht
 */
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

abstract public class AbstractBookActions {
    /**
     * Etykieta dla nowego okna.
     */
    @FXML
    Label label;
    /**
     * Pole tekstowe, w którym jest ID książki.
     */
    @FXML
    TextField id;
    /**
     * Pole tekstowe, w którym jest numer ISBN książki.
     */
    @FXML
    TextField isbn;
    /**
     * Pole tekstowe, w którym znajduje się autor książki.
     */
    @FXML
    TextField author;
    /**
     * Pole tekstowe, w którym znajduje się tytuł książki.
     */
    @FXML
    TextField title;
    /**
     * Pole tekstowe, w którym znajduje się rok wydania książki.
     */
    @FXML
    TextField year;
    /**
     * Pole wyboru stanu zewnętrznego książki.
     */
    @FXML
    ComboBox<String> condition;
    /**
     * Pole wyboru gatunku książki.
     */
    @FXML
    ComboBox<String> kind;
    /**
     * Przycisk wywołujący odpowiednią akcje.
     */
    @FXML
    Button Button;
    /**
     * Opis dla zakladki comboBox.
     */
    @FXML
    Label status;
    /**
     * Wybór statusu.
     * 
     */
    @FXML
    ComboBox<Status> statusComboBox;
}
