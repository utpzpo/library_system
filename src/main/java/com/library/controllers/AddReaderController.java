/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import static com.library.controllers.MainController.logger;
import com.library.sql.SQLController;
import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ObservableBooleanValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author kucht
 */
public class AddReaderController extends ReaderController{

    
    public static boolean isAdded=false;
    public AddReaderController() throws IOException 
    {
        
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AddUser.fxml"));
        loader.setController(this);
        root= (Parent) loader.load();
        Stage stage = new Stage();
        stage.setTitle("Dodaj użytkownika");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
        logger.info("Wywołanie okna "+this.getClass().toString());
    }
    
    
    
    @FXML
    @Override
    public void onAddReaderAction(ActionEvent event) {
        SQLController.getInstance().addReader(name.getText().trim().toUpperCase(), 
                surname.getText().trim().toUpperCase(), 
                PESEL.getText().trim().toUpperCase(), 
                documentType.getSelectionModel().getSelectedItem().toUpperCase(), 
                documentNumber.getText().trim().toUpperCase(), 
                email.getText().trim().toUpperCase(), 
                phoneNumber.getText().trim().toUpperCase(), 
                street.getText().trim().toUpperCase(), 
                city.getText().trim().toUpperCase());
        isAdded=true;
    }
    
    public void initialize()
    {
        addReader.setText("Dodaj!");
        id.setText(String.valueOf(SQLController.getInstance().getLastIndexOfReader()));
        documentType.setItems(FXCollections.observableArrayList(
        SQLController.getInstance().getAllDocumentTypes()));
    }
}
