/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.User;
import com.library.sql.SQLController;
import java.util.List;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class UsersController{
    /**
     * fx:id="userList
     * Używane do wyświetlania użytkowników w liście
     * Wartość wstrzyknięta przez FXMLLoader
     */
    @FXML
    private ComboBox<User> userList;
    /**
     * fx:id="add"
     * Button dodaj użytkownika.
     * Wartość wstrzyknięta przez FXMLLoader.
     */
    @FXML
    private Button add; // Value injected by FXMLLoader

     /**
     * fx:id="close"
     * Button zamknij okno.
     * Wartość wstrzyknięta przez FXMLLoader
     */
    @FXML 
    private Button close; // Value injected by FXMLLoader
    /**
     * fx:id="loginField"
     * Pole do wprowadzenia loginu.
     * Wartość wstrzyknięta przez FXMLLoader.
     */
    @FXML 
    private TextField loginField; // Value injected by FXMLLoader
    /**
     * fx:id="passField"
     * Pole do wprowadzenia hasła.
     * Wartość wstrzyknięta przez FXMLLoader.
     */
    @FXML
    private PasswordField passField; // Value injected by FXMLLoader
    /**
     * fx:id="confirmPassField"
     * Pole do potwierdzenia hasła.
     * Wartość wstrzyknięta przez FXMLLoader.
     */
    @FXML
    private PasswordField confirmPassField; // Value injected by FXMLLoader
    /**
     * Obserwator listy użytkowników.
     * @see ObservableList
     */
    private ObservableList<User> usersList;
    /**
     * Właściwość dla listy z userami.
     * @see ListProperty
     */
    private ListProperty<User> userProperty;
    /**
     * Pojedynczy obiekt usera.
     */
    private User user;
    /**
     * Funkcja zapisuje użytkownika do bazy po uprzednim sprawdzeniu, czy hasła się zgadzają.
     * W przypadku, gdy hasła się nie zgadzają, wyświetlany jest alert i połączenie z bazą nie jest nawiązywane.
     * Gdy dane są poprawne, pojawia się alert o poprawnym przetworzeniu żądania.
     * @see SQLController
     * @param event 
     */
    @FXML
    void onAddActionButton(ActionEvent event) {
        if(passField.getText().equals(confirmPassField.getText()))
        {
            SQLController.getInstance().addUser(loginField.getText().trim(), passField.getText().trim());
            usersList.add(new User(loginField.getText()));
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Dodano użytkownika");
            alert.setHeaderText("Pomyślnie dodano użytkownika do bazy!");
            alert.showAndWait();
        }
        else
        {
            SQLController.logger.debug("Próba dodania użytkownika zakończyła się błędem. Hasła nie są zgodne.");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Nieudana próba dodania użytkownika");
            alert.setHeaderText("Hasła nie są jednakowe!");
            alert.showAndWait();
            passField.setStyle("-fx-border-width:1px");
            confirmPassField.setStyle("-fx-border-width:1px");
        }
    }
    /**
     * Zamyka okienko kontrolowane przez UsersController.
     * @param event - cliknięcie przycisku zamknij
     */
    @FXML
    void onCloseActionButton(ActionEvent event) {
        Stage stage = (Stage) close.getScene().getWindow();
        stage.close();
    }
    /**
     * Oznacza wciśnięcie klawisza Enter w formularzu. Dodaje użytkownika do bazy.
     * @see onAddActionButton(ActionEvent event)
     * @param event - wywołanie akcji
     */
    @FXML
    void enterAction(ActionEvent event) {
        onAddActionButton(event);
    }
    /**
     * Funkcja initialize jest wywoływana po wywołaniu wszystkich
     * pól z adnotacją @FXML.
     * Służy do zainicjowania widoku.
     */
    @FXML
    public void initialize()
    {
        List users = SQLController.getInstance().getUsers();
        userProperty = new SimpleListProperty();
        usersList = FXCollections.observableArrayList(users);
        userProperty.set(usersList);
        userList.itemsProperty().bindBidirectional(userProperty);
    }
    

}
