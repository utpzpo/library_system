/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.sql.SQLController;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author kucht
 */
public class EditBookController extends AbstractBookActions{
    
    public EditBookController() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AddBook.fxml"));
        loader.setController(this);
        Parent root= (Parent) loader.load();
        Stage stage = new Stage();
        stage.setTitle("Edit User");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }
    @FXML
    void onActionButton(ActionEvent event) {
        SQLController.getInstance().editBook(Integer.valueOf(id.getText()),
                isbn.getText().trim().toUpperCase(),
                author.getText().trim().toUpperCase(),
                title.getText().trim().toUpperCase(),
                year.getText().trim().toUpperCase(),
                condition.getSelectionModel().getSelectedItem(),
                kind.getSelectionModel().getSelectedItem(),
                statusComboBox.getSelectionModel().getSelectedItem().status()
                );
    }
    
    @FXML
    void initialize()
    {
        Button.setText("Zapisz");
        Button.setDisable(true);
        status.setDisable(false);
        kind.setItems(FXCollections.observableArrayList(SQLController.getInstance().getAllKinds()));
        condition.setItems(FXCollections.observableArrayList(SQLController.getInstance().getAllConditions()));
        statusComboBox.setDisable(false);
        condition.setItems(FXCollections.observableArrayList(Status.DOSTEPNY.status(),Status.NIEDOSTEPNY.status()));
                
        String idText=id.getText();
        String isbnText=isbn.getText();
        String authorText=author.getText();
        String titleText=title.getText();
        String yearText=year.getText();
        String conditionText=condition.getSelectionModel().getSelectedItem();
        String kindText=condition.getSelectionModel().getSelectedItem();
        String statusComboBoxText=statusComboBox.getSelectionModel().getSelectedItem().status();
        id.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(idText.equals(newValue))
                Button.setDisable(false);
        });
        isbn.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(isbnText.equals(newValue))
                Button.setDisable(false);
        });
        author.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(authorText.equals(newValue))
                Button.setDisable(false);
        });
        title.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(titleText.equals(newValue))
                Button.setDisable(false);
        });
        year.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(year.toString().equals(newValue))
                Button.setDisable(false);
        });
        condition.promptTextProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(condition.toString().equals(newValue))
                Button.setDisable(false);
        });
        kind.promptTextProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.isEmpty())
                Button.setDisable(false);
            if(kind.toString().equals(newValue))
                Button.setDisable(false);
        });
        statusComboBox.promptTextProperty().addListener((observable, oldValue, newValue) -> {
            if(statusComboBoxText.equals(kindText))
                Button.setDisable(false);
        });
    }
    
}
