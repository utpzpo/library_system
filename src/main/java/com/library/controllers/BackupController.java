/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

/**
 * Kontroler odpowiedzialny za dodawanie, edycję i usuwanie użytkowników
 * aplikacji.
 * 
 * @author kucht
 */

import Files.FileExplorer;
import com.library.sql.SQLController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class BackupController {
    /**
     * Przycisk obsługuje menedżer plików dla systemu.
     * Należy wybrać folder i wpisać nazwę pliku, do którego zapisze się baza.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button ChooseArchiveLocation;
    /**
     * Rozpoczyna archiwizację bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button Archive;
    /**
     * Przycisk obsługujący menedżer plików dla systemu.
     * Należy wybrać plik, z którego będzie odtwarzana baza.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button ChooseWhereArchive;
    /**
     * CheckBox, który oznacza, że baza będzie uruchamiana zaraz po 
     * uruchomieniu programu.
     * Value injected by FXMLLoader
     */
    @FXML
    private CheckBox AutoOpenCheckBox;
    /**
     * CheckBox, który oznacza, że odtworzenie pliku nadpisze aktualny plik
     * bazy.
     * Value injected by FXMLLoader
     */
    @FXML
    private CheckBox OverrideFileCheckBox;
    /**
     * Rozpoczyna odtwarzanie bazy.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button OpenBase;
    /**
     * Przycisk zamyka okno od backupu.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button CloseWindow;
    /**
     * Pole tekstowe, w którym znajduje się ścieżka, w którym baza
     * będzie zarchiwizowana.
     */
    @FXML
    private TextField archiveDest;
    /**
     * Pole tekstowe, w którym znajduje się ścieżka, z której zostanie odtworzona
     * baza.
     */
    @FXML
    private TextField openBase;
    /**
     * Archiwizuje bazę w lokacji podanej w polu archiveDest. 
     * @param event event wywołujący funkcję (tu wciśnięcie przycisku)
     * @throws FileNotFoundException 
     */

    @FXML
    void onArchiveActionButton(ActionEvent event) throws FileNotFoundException, IOException {
        FileChannel sourceChannel = null;
        FileChannel destinaChannel = null;
            try {
        sourceChannel = new FileInputStream(SQLController.getInstance().URL).getChannel();
        destinaChannel = new FileOutputStream(archiveDest.getText()).getChannel();
        destinaChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
       }finally{
           sourceChannel.close();
           destinaChannel.close();
   }
    }

    @FXML
    void onChooseArchiveLocationActionButton(ActionEvent event) throws Exception {
        new FileExplorer().start(new Stage());
    }

    @FXML
    void onChooseWhereArchiveActionButton(ActionEvent event) throws Exception {
        new FileExplorer().start(new Stage());
    }

    @FXML
    void onCloseWindow(ActionEvent event) {
        Stage stage = (Stage) CloseWindow.getScene().getWindow();
        stage.close();
    }

    @FXML
    void onOpenBaseActionButton(ActionEvent event) {
        SQLController.getInstance().URL=openBase.getText();
    }

}
