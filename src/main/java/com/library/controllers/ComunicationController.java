/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.BooksItem;
import com.library.entities.ReaderBorrow;
import com.library.sql.SQLController;
import com.library.verifier.PeselValidator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kucht
 */
public class ComunicationController {
    public static Logger logger = LoggerFactory.getLogger(MainController.class);
    /**
     * fx:id"KReaderID"
     * Pole tekstowe, w którym należy wpisać numer czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KReaderID;
    /**
     * fx:id="KName"
     * Pole tekstowe, w którym znajduje się imię czytelnika, do którego
     * jest wysyłany komunikat.
     */
    @FXML
    private TextField KName;
    /**
     * fx:id="KSurname"
     * Pole tekstowe, w którym znajduje się nazwisko czytelnika, do którego
     * jest wysyłąny komunikat.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KSurname;
    /**
     * fx:id="KBirthDate"
     * Pole tekstowe, w którym znajduje się data urodzenia czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KBirthDate;
    /**
     * fx:id="KDocumentID"
     * Pole tekstowe, w którym znajduje się numer dokumentu.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KDocumentID;
    /**
     * fx:id="KDocumentType"
     * Pole tekstowe, w którym znajduje się typ dokumentu.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KDocumentType;
    /**
     * fx:id="KPESEL"
     * Pole tekstowe, w którym znajduje się numer PESEL czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KPESEL;
    /**
     * fx:id="KPhoneNumber"
     * Pole tekstowe, w którym znajduje się numer telefonu czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KPhoneNumber;
    /**
     * fx:id="KStreet"
     * Pole tekstowe, w którym znajduje się nazwa ulicy, na której zamieszkały
     * jest czytelnik.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KStreet; 
    /**
     * fx:id="KCity"
     * Pole tekstowe, w którym znajduje się miejscowość, w której zamieszkały
     * jest czytelnik.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KCity; 
    /**
     * fx:id="KRentedBooks"
     * Lista, która zawiera wszystkie aktualne wypożyczenia czytelnika.
     * Value injected by FXMLLoader.
     */
    @FXML 
    private ComboBox<BooksItem> KRentedBooks;
    /**
     * fx:id="KEmail"
     * Pole tekstowe, które zawiera mail.
     * Value injected by FXMLLoader.
     */
    @FXML
    private TextField KEmail;
    /**
     * ComboBox. Lista książek, które uległy przedawnieniu
     */
    @FXML
    private ComboBox<BooksItem> KNumberOfOverdueBooks;
    /**
     * fx:id="KConfirmedEmail"
     * Pole tekstowe, które zawiera mail do potwierdzenia.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KConfirmedEmail; 
    /**
     * fx:id="KConfirm"
     * Przycisk zatwierdzający email, na który zostanie wysłany komunikat.
     * Po wciśnieciu przycisku, pole email nie jest już możliwe do edycji.
     * Value injected by FXMLLoader
     */
    @FXML 
    private Button KConfirm;
    /**
     * fx:id="KTemplates"
     * Lista zawierająca szablony wiadomości możliwe do wykorzystania.
     * Value injected by FXMLLoader
     */
    @FXML
    private ChoiceBox<?> KTemplates;
    /**
     * fx:id="KSubject"
     * Pole tekstowe, w którym należy wpisać temat wiadomości.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KSubject; 
    /**
     * fx:id="KMessageContent"
     * Pole tekstowe, w którym należy wpisać treść wiadomości.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextArea KMessageContent;
    /**
     * fx:id="KSend"
     * Przycisk wysyłający maila do adresata podanego w polu KConfirmEmail.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button KSend;
    /**
     * fx:id="Clear"
     * Przycisk czyszczący wszystkie pola w zakładce Komunikaty.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button Clear;
    /**
     * Kontener, w którym są wszystkie elementy.
     */
    @FXML
    private HBox main;
     /**
     * Czyści pola tekstowe w zakładce komunikaty.
     */
    @FXML
    void onClearActionButton(ActionEvent event)
    {
        KNumberOfOverdueBooks.setItems(null);
        KRentedBooks.setItems(null);
        KConfirmedEmail.setEditable(true);
        main.getChildren().stream()
                .filter(node -> (node instanceof TextField))
                .forEach((node -> {
                    ((TextField) node).clear();
                }));
        
    }
    /**
     * Potwierdzenie email, na który zostanie wysłany komunikat.
     * Uniemożliwia edycję pola.
     * @param event wciśnięcie przycisku KConfirm
     */
    @FXML
    void onKConfirmActionButton(ActionEvent event) {
        KConfirmedEmail.setEditable(false);
    }
    /**
     * Wysyła mail do osoby w polu KConfirmedEmail.
     * Funkcja używa properties z pliku mail.properties.
     * 
     * @param event event
     */
    @FXML
    void onKSendActionButton(ActionEvent event) {
        System.setProperty("file.encoding", "UTF-8");
        System.out.println(MainController.class.getResource("/properties"));
        File f = new File(MainController.class.getResource("/properties/mail.properties").toString());
        Properties properties = new Properties();
        try {
            InputStream is = new FileInputStream(f);
            properties.load(is);
            String host = properties.getProperty("serwer");
            String port = properties.getProperty("port");
            String from = properties.getProperty("mail");
            String password = properties.getProperty("password");
            String subject = KSubject.getText().trim();
            String content = KMessageContent.getText();
            String to = KConfirmedEmail.getText();
            Properties props = new Properties();
            props.put("mail.transport.protocol", "smtps");
            props.put("mail.smtps.auth","true");
            //Inicjalizujemy sesję
            Session mailSession = Session.getDefaultInstance(props);
            //ustawienie debugowania
            mailSession.setDebug(true);
            //tworzenie wiadomości
            MimeMessage message = new MimeMessage(mailSession);
            //ustawienie tematu wiadomości
            message.setSubject(subject);
            //ustawinie treści
            message.setContent(content, "text/html; charset=ISO-8859-2");
            //dodanie, skąd email zostanie wysłany
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            //tworzenie połączenia z serwerem pocztowym
            Transport transport = mailSession.getTransport();
            transport.connect(host, Integer.valueOf(port), from, password);
            //wysłanie wiadomości
            logger.info("Wysłanie wiadomości na adres :"+KConfirmedEmail.getText().trim());
            transport.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            //zamknięcie połączenia
            transport.close();
            //archiwizowanie komunikatu
            SQLController.getInstance().archiveCommunicate
        (Integer.valueOf(KReaderID.getText()), content);
        } catch (FileNotFoundException ex) {
            logger.error("Nie odnaleziono pliku mail.properties !"+ex.getMessage());
        } catch (IOException ex) {
            logger.error("Wystąpił problem: " + ex.getMessage());
        } catch (MessagingException ex) {
            logger.error("Nie udane wysłanie wiadomości. Szczegóły:\n"+ex.getMessage());
        }
    }
    /**
     * Ustawia czytelnika, do którego zostanie wysłany komunikat.
     * @param rb - obiekt czytelnika
     */
    void setReader(ReaderBorrow rb)
    {
        KReaderID.setText(String.valueOf(rb.getId()));
                PeselValidator pv = new PeselValidator(rb.getPESEL());
        KBirthDate.setText(pv.getBirthDayDate());
        KCity.setText(rb.getCity());
        KEmail.setText(rb.getEmail());
        KDocumentID.setText(rb.getDocumentNumber());
        KDocumentType.setText(rb.getDocumentType());
        KName.setText(rb.getName());
        KNumberOfOverdueBooks.setItems(FXCollections.observableArrayList(
                SQLController.getInstance().getOverduedBooks(rb.getId())));
        KPESEL.setText(rb.getPESEL());
        KPhoneNumber.setText(rb.getPhoneNumber());
        KStreet.setText(rb.getStreet());
        KSurname.setText(rb.getSurname());
        KRentedBooks.setItems(FXCollections.observableArrayList(
                SQLController.getInstance().getActiveRentedBooks(rb.getId())));
        KConfirmedEmail.setText(KEmail.getText());
    }
}
