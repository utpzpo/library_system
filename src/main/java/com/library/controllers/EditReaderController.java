/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.sql.SQLController;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author kucht
 */
public class EditReaderController extends ReaderController{
    
    public static int readerID;
    
    public static Boolean isAdded=false;

    public EditReaderController() throws IOException 
    {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AddUser.fxml"));
        loader.setController(this);
        root= (Parent) loader.load();
        Stage stage = new Stage();
        stage.setTitle("Edit User");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }
    
    
    @FXML
    @Override
    public void onAddReaderAction(ActionEvent event) {
        SQLController.getInstance().addReader(name.getText().trim().toUpperCase(), 
                surname.getText().trim().toUpperCase(), 
                PESEL.getText().trim().toUpperCase(), 
                documentType.getSelectionModel().getSelectedItem().toUpperCase(), 
                documentNumber.getText().trim().toUpperCase(), 
                email.getText().trim().toUpperCase(), 
                phoneNumber.getText().trim().toUpperCase(), 
                street.getText().trim().toUpperCase(), 
                city.getText().trim().toUpperCase());
        
    }
    @FXML
    public void initialize(){
        addReader.setText("Edytuj");
        id.setText(String.valueOf(readerID));
    }
    
}
