/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.sql.SQLController;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author kucht
 */
public class AddBookController extends AbstractBookActions {

    public AddBookController() throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/AddBook.fxml"));
        loader.setController(this);
        Parent root= (Parent) loader.load();
        Stage stage = new Stage();
        stage.setTitle("Edit User");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
    }
    
    
    @FXML
    void onActionButton(ActionEvent event) {
        SQLController.getInstance().addBook(Integer.valueOf(id.getText()),
                isbn.getText().trim().toUpperCase(),
                author.getText().trim().toUpperCase(),
                title.getText().trim().toUpperCase(),
                year.getText().trim().toUpperCase(),
                condition.getSelectionModel().getSelectedItem().toString(),
                kind.getSelectionModel().getSelectedItem().toString(),
                "Dostępny");
    }
    
    @FXML
    void initialize()
    {
        Button.setText("Dodaj!");
        status.setDisable(true);
        statusComboBox.setDisable(true);
    }
}
