package com.library.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

/**
 * Kontroler dla dodawania nowego czytelnika.
 * @author kucht
 */
public abstract class ReaderController extends AnchorPane {
    /**
     * ID czytelnika.
     */
    @FXML
    TextField id;
    /**
     * Imię czytelnika.
     */
    @FXML
     TextField name;
    /**
     * Nazwisko czytelnika.
     */
    @FXML
    TextField surname;
    /**
     * PESEL czytelnika.
     */
    @FXML
     TextField PESEL;
    /**
     * Data urodzenia czytelnika.
     */
    @FXML
    TextField dateOfBirth;
    /**
     * Typ documentu czytelnika.
     */
    @FXML
    ComboBox<String> documentType;
    /**
     * Nr dokumentu czytelnika.
     */
    @FXML
    TextField documentNumber;
    /**
     * Email czytelnika.
     */
    @FXML
    TextField email;
    /**
     * Numer telefonu do czytelnika.
     */
    @FXML
    TextField phoneNumber;
    /**
     * Ulica, na której zamieszkały jest czytelnik.
     */
    @FXML
    TextField street;
    /**
     * Miejscowość, w której zamieszkały jest czytelnik.
     */
    @FXML
    TextField city;
    /**
     * Dodaje użytkownika do bazy danych.
     */
    @FXML
    Button addReader;
    
    Parent root;
    public ReaderController()
    {
        
    }
    @FXML
    public void onAddReaderAction(ActionEvent event)
    {
        
    }

}
