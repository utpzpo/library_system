package com.library.controllers;

import com.library.reports.ReportGenerator;
import com.lowagie.text.pdf.BarcodeEAN;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.stage.Stage;
import net.sf.dynamicreports.report.exception.DRException;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.tools.UnitConv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainController {

    /**
     * Logger dla klasy MainController.
     */
    public static Logger logger = LoggerFactory.getLogger(MainController.class);
    /**
     * Zwraca referencję do głównego kontrolera. 
     */
    public static MainController instance;
    public static MainController getMainInstance()
    {
        return instance;
    }
    /**
     * Referencja do kontrolera BorrowController.
     */
    @FXML
    BorrowController borrowController;
    /**
     * Referencja do kontrolera ReaderController.
     */
    @FXML
    ReadersController readerController;
    /**
     * Referencja do kontrolera BookController.
     */
    @FXML
    BookController bookController;
    /**
     * Referencja do kontrolera ReturnController.
     */
    @FXML
    ReturnController returnController;
    /**
     * Referencja do kontrolera ComunicationController.
     */
    @FXML
    ComunicationController comunicationController;
    /**
     * Getter dla kontrolera BorrowController.
     * @return referencja do BorrowController.
     */
    public BorrowController getBorrowController()
    {
        return borrowController;
    }
    public ReadersController getReaderController()
    {
        return readerController;
    }
    public BookController getBookController()
    {
        return bookController;
    }
    public ReturnController getReturnController()
    {
        return returnController;
    }
    public ComunicationController getComunicationController()
    {
        return comunicationController;
    }
    public void setBorrowController(BorrowController bc)
    {
        this.borrowController=bc;
    }
    public void setReaderController(ReadersController rc)
    {
        this.readerController=rc;
    }
    public void setBookController(BookController bc)
    {
        this.bookController=bc;
    }
    public void setReturnController(ReturnController rc)
    {
        this.returnController=rc;
    }
    public void setComunicationController(ComunicationController cc)
    {
        this.comunicationController=cc;
    }
    /**
     * 
     * fx:id="Users"
     * Element menu odpowiedzialny za wyświetlenie okienka z użytkownikami.
     * Value injected by FXMLLoader
     */
    @FXML
    private MenuItem Users; 
    /**
     * fx:id="Backup"
     * Element menu odpowiedzialny za wyświetlenie okienka z tworzeniem kopii
     * zapasowej bazy.
     * @see UsersController
     * Value injected by FXMLLoader.
     */
    @FXML
    private MenuItem Backup;
    /**
     * fx:id="CloseProgram"
     * Element menu odpowiedzialny za zamknięcie programu.
     * @see BackupController
     * Value injected by FXMLLoader.
     */
    @FXML
    private MenuItem CloseProgram;
    /**
     * fx:id="ActiveRentedBooks"
     * Element menu odpowiedzialny za wygenerowanie raportu 
     * wszystkich aktywnych wypożyczeń.
     * Value injected by FXMLLoader
     */
    @FXML
    private MenuItem ActiveRentedBooks;
    /**
     * fx:id="ActiveBorrowers"
     * Element menu odpowiedzialny za wygenerowanie raportu
     * dla wszystkich czytelników, którzy mają na swoim koncie
     * wypożyczone książki.
     * Value injected by FXMLLoader
     */
    @FXML
    private MenuItem ActiveBorrowers;
    /**
     * fx:id="ReportOfRentedBooksPerDay"
     * Element menu odpowiedzialny za wygenerowanie raportu
     * ilość wypożyczeń książek na dzień.
     * Value injected by FXMLLoader
     */
    @FXML
    private MenuItem ReportOfRentedBooksPerDay; 
    /**
     * fx:id="Categories"
     * Element menu odpowiedzialny za wyświetlenie słownika kategorii.
     * Value injected by FXMLLoader
     */
    @FXML 
    private MenuItem Categories; 
    /**
     * fx:id="UKDDictionary"
     * Element menu odpowiedzialny za uniwersalną klasyfikację dziesiętną,
     * konieczną dla rzetelnego prowadzenia biblioteki.
     * Value injected by FXMLLoader
     */
    @FXML
    private MenuItem UKDDictionary;
    /**
     * fx:id="ReasonsForDefects"
     * Element menu odpowiedzialny za wyświetlenie nowego okna
     * ze słownikiem odzwierciedlającym powody ubytków.
     * Value injected by FXMLLoader.
     */
    @FXML 
    private MenuItem ReasonsForDefects;
    /**
     * fx:id="PrintBookLabel"
     * Element menu odpowiedzialny za wydruk etykiety z kodem kreskowym
     * identyfikujący konkretny egzemplarz książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    private MenuItem PrintBookLabel; 
    /**
     * fx:id="PrintCardOfLoans"
     * Element menu odpowiedzialny za wydruk karty wypożyczeń dla 
     * roku kalendarzowego dla konkretnego egzemplarza książki.
     * Value injected by FXMLLoader
     */
    @FXML
    private MenuItem PrintCardOfLoans;
    /**
     * fx:id="GeneralOptions"
     * Element menu odpowiedzialny za wyświetlenie nowego okna,
     * do zmiany czcionki, motywu obiektu.
     * Value injected by FXMLLoader.
     */
    @FXML 
    private MenuItem GeneralOptions;
    /**
     * fx:id="KBooks"
     * Zakładka Książka w głównym oknie aplikacji.
     * Value injected by FXMLLoader
     */
    @FXML
    private Tab KBooks;
    /**
     * fx:id="CReaders"
     * Zakładka Czytelnik w głównym oknie aplikacji.
     * Value injected by FXMLLoader
     */
    @FXML 
    private Tab CReaders;
    /**
     * fx:id="WLoans"
     * Zakładka Wypożyczenia.
     * Value injected by FXMLLoader
     */
    @FXML
    private Tab WLoans;
    /**
     * fx:id="ZReturns"
     * Zakładka zwroty.
     * Value injected by FXMLLoader
     */
    @FXML
    private Tab ZReturns;

    /**
     * fx:id="Communication"
     * Zakładka Komunikaty
     * Value injected by FXMLLoader
     */
    @FXML 
    private Tab Communication;
    /**
     * Wywołuję funkcję, która tworzy raport aktywnych czytelników posiadających
     * wypożyczone książki.
     * @see ReportGenerator
     * @param event - event wywołujący funkcję
     * @throws SQLException
     * @throws MalformedURLException
     * @throws ClassNotFoundException 
     */
    @FXML
    void onActiveBorrowersMenuAction(ActionEvent event) throws SQLException, MalformedURLException, ClassNotFoundException {
        logger.info(event.getEventType().toString() + "Wywołanie raportu czytelników, którzy mają na swoim koncie wypożyczenia.");
        ReportGenerator.buildActiveBorrowersReport();
    }
    /**
     * Wywołuje funkcję, która tworzy raport aktywnych wypożyczeń.
     * @see ReportGenerator
     * @param event
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws MalformedURLException 
     */
    @FXML
    void onActiveRentedBooksMenuAction(ActionEvent event) throws ClassNotFoundException, SQLException, MalformedURLException {
        logger.info(event.getEventType().toString() + "Wywołanie raportu aktywnych wypożyczeń");
        ReportGenerator.buildActiveBorrowsReport();
    }
    /**
     * Otwiera nowe okno do tworzenia backupu bazy.
     * @param event
     * @throws IOException 
     */
    @FXML
    void onBackupMenuAction(ActionEvent event) {
        Parent root=null;
        try {
            root = FXMLLoader.load(getClass().getResource("/fxml/Backup.fxml"));
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
        Stage stage = new Stage();
        stage.setTitle("Utwórz kopię zapasową");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
        logger.info(event.getEventType().toString()+ "Wywołanie okna ");
    }
    @FXML
    void onUsersMenuAction(ActionEvent event) throws IOException {
        Parent root= FXMLLoader.load(getClass().getResource("/fxml/Users.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Użytkownicy");
        stage.setScene(new Scene(root));
        stage.setResizable(false);
        stage.show();
        logger.info(event.getEventType().toString()+ "Wywołanie okna ");
    }
    @FXML
    void onCategoriesMenuAction(ActionEvent event) throws IOException {
        try
        {
            new KindDictionaryController();
        }
        catch(IOException ex)
        {
            logger.error(ex.getMessage()); 
        }
    }

    @FXML
    void onCloseProgramMenuAction(ActionEvent event) {
        Platform.exit();
    }

    @FXML
    void onGeneralOptionsMenuAction(ActionEvent event) {
        
    }

    @FXML
    void onPrintBookLabelActionButton(ActionEvent event) {
        final int dpi = 100;
        Code128Bean code128Bean = new Code128Bean();
        code128Bean.setBarHeight(2.2);
        code128Bean.setModuleWidth(UnitConv.in2mm(5.0f / dpi));
        code128Bean.doQuietZone(false);
        File outputFile;
        BarcodeEAN barcodeEAN = new BarcodeEAN();
        barcodeEAN.setCodeType(barcodeEAN.EAN8);
        
        
    }

    @FXML
    void onPrintCardOfLoansMenuAction(ActionEvent event) {

    }

    @FXML
    void onReasonForDefectsMenuAction(ActionEvent event) {
        try
        {
            new DefectsDictionaryController();
        }
        catch(IOException ex)
        {
            logger.error(ex.getMessage());
        }
    }

    @FXML
    void onReportOfRentedBooksPerDayMenuAction(ActionEvent event) {
        String startDate="",endDate="";
        try
        {ReportGenerator.borrowsPerDayOfMonthReport(startDate, endDate);
        }
        catch(ClassNotFoundException | DRException | MalformedURLException | SQLException ex)
        {
            logger.error(ex.getMessage());
        }
    }

    @FXML
    void onUKDDictionaryMenuAction(ActionEvent event) {
        try
        {
            new UKDDictionaryController();
        }
        catch(IOException ex)
        {
            logger.error(ex.getMessage());
        }
    }
    @FXML
    void initialize()
    {
        readerController.setBorrowController(borrowController);
        readerController.setComunicationController(comunicationController);
        readerController.setReturnController(returnController);
        bookController.setBorrowController(borrowController);
        bookController.setComunicationController(comunicationController);
        bookController.setReturnController(returnController);
    }


}
