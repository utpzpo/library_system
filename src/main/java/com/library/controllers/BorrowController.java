/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.BookBorrow;
import com.library.entities.ReaderBorrow;
import com.library.sql.SQLController;
import com.library.verifier.PeselValidator;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kucht
 */
public class BorrowController {
    public static Logger logger = LoggerFactory.getLogger(MainController.class);
     /**
     * fx:id="WBookID"
     * Pole tekstowe, w którym należy wpisać ID wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WBookID;
    /**
     * fx:id="WISBN"
     * Pole tekstowe, w którym należy wpisać ISBN wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WISBN;
    /**
     * fx:id="WAuthor"
     * Pole tekstowe, w którym należy wpisać Autora wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField WAuthor;
    /**
     * fx:id="WTitle"
     * Pole tekstowe, w którym należy wpisać Tytuł wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WTitle;
    /**
     * fx:id="WYear"
     * Pole tekstowe, w którym należy wpisać Rok wydania wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WYear;
    /**
     * fx:id="WCondition"
     * Pole tekstowe, w którym należy wpisać Stan wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WCondition;
    /**
     * fx:id="WKind"
     * Pole tekstowe, w którym należy wpisać Rodzaj(Kategorię) wypożyczanej książki.
     * Docelowo przekazywane z zakładki Książki.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WKind;
    /**
     * fx:id="WSearchForBook"
     * Przycisk wywołujący szukanie w bazie książki po numerze inwentarzowym.
     * Value injected by FXMLLoader
     */
    @FXML
    Button WSearchForBook;
    /**
     * fx:id="WReaderID"
     * Pole tekstowe, w którym należy wpisać numer czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WReaderID;
    /**
     * fx:id="WName"
     * Pole tekstowe, w którym należy wpisać imię czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WName;
    /**
     * fx:id="WSurname"
     * Pole tekstowe, w którym należy wpisać nazwisko czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WSurname;
    /**
     * fx:id="WBirthDate"
     * Pole tekstowe, w którym umieszczana jest data urodzenia czytelnika.
     * Obliczane na podstawie nr PESEL.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML 
    TextField WBirthDate;
    /**
     * fx:id="WDocumentID"
     * Pole tekstowe, w którym należy wpisać numer dokumentu czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WDocumentID;
    /**
     * fx:id="WDocumentType"
     * Pole tekstowe, w którym należy wpisać typ dokumentu czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WDocumentType;
    /**
     * fx:id="WPESEL"
     * Pole tekstowe, w którym należy wpisać PESEL czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z zakładki Czytelnicy.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WPESEL;
    /**
     * fx:id="WMail"
     * Pole tekstowe, w którym należy wpisać email czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WMail;
    /**
     * fx:id="WPhoneNumber"
     * Pole tekstowe, w którym należy wpisać numer telefonu czytelnika, który wypożycza
     * daną książkę.
     * Docelowo przekazywane z bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WPhoneNumber;
    /**
     * fx:id="WStreet"
     * Pole tekstowe, w którym należy wpisać ulicę, na której mieszka czytelnik
     * wypożyczający książkę.
     * Docelowo przekazywane z bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WStreet;
    /**
     * fx:id="WCity"
     * Pole tekstowe, w którym należy wpisać miejscowość zamieszkania czytelnika,
     * który wypożycza książkę.
     * Docelowo przekazywane z bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WCity;
    /**
     * fx:id="WSearchForReader"
     * Przycisk wywołujący szukanie po numerze czytelnika.
     * Value injected by FXMLLoader.
     */
    @FXML
    Button WSearchForReader;
    /**
     * fx:id=:WactuallyBorrowedBooks
     * Przycisk obsługujący wyświetlenie nowego okna ukazujących aktualne wypożyczenia
     * dla danego czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    Button WActuallyBorrowedBooks;
    /**
     * fx:id="WRent"
     * Przycisk, który służy do zatwierdzenia wypożyczenia przez danego
     * czytelnika.
     * Value injected by FXMLLoader
     */
    @FXML
    Button WRent;
    /**
     * fx:id="WDateOfRent"
     * Pole tekstowe, w którym pojawia się dzisiejsza data.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WDateOfRent;
    /**
     * fx:id="WTime"
     * Pole tekstowe, w którym należy wpisać okres wypożyczenia książki 
     * czytelnikowi (w dniach).
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WTime;
    /**
     * fx:id="WPlannedReturn"
     * Pole tekstowe, w którym jest obliczona data oddania na podstawie
     * liczby dni w polu tekstowym wyżej.
     * Value injected by FXMLLoader
     */
    @FXML
    TextField WPlannedReturn;
    /**
     * Właściwość dla pola id.
     */
    private IntegerProperty id = new SimpleIntegerProperty();
    /**
     * Właściwość dla pola imię.
     */
    private StringProperty name = new SimpleStringProperty();
    /**
     * Właściwość dla pola nazwisko.
     */
    private StringProperty surname = new SimpleStringProperty();
    /**
     * Właściwość dla pola data urodzenia.
     */
    private StringProperty dateOfBirth = new SimpleStringProperty();
    /**
     * Właściwość dla pola PESEL.
     */
    private StringProperty PESEL = new SimpleStringProperty();
    /**
     * Właściwość dla pola numer dokumentu.
     */
    private StringProperty documentID = new SimpleStringProperty();
    /**
     * Właściwość dla pola typ dokumentu.
     */
    private StringProperty documentType = new SimpleStringProperty();
    /**
     * Właściwość dla pola email.
     */
    private StringProperty email = new SimpleStringProperty();
    /**
     * Właściwość dla pola numer telefonu.
     */
    private StringProperty phoneNumber = new SimpleStringProperty();
    /**
     * Właściwosć dla pola ulica.
     */
    private StringProperty street = new SimpleStringProperty();
    /**
     * Właściwość dla pola miejscowość.
     */
    private StringProperty city = new SimpleStringProperty();

//    public int getId() {
//        return id.get();
//    }
//
//    public void setId(int id) {
//        this.id.set(id);
//    }
//
//    public String getName() {
//        return name.get();
//    }
//
//    public void setName(String name) {
//        this.name.set(name);
//    }
//
//    public String getSurname() {
//        return surname.get();
//    }
//
//    public void setSurname(String surname) {
//        this.surname.set(surname);
//    }
//
//    public String getDateOfBirth() {
//        return dateOfBirth.get();
//    }
//
//    public void setDateOfBirth(String dateOfBirth) {
//        this.dateOfBirth.set(dateOfBirth);
//    }
//
//    public String getDocumentID() {
//        return documentID.get();
//    }
//
//    public void setDocumentID(String documentID) {
//        this.documentID.set(documentID);
//    }
//
//    public String getDocumentType() {
//        return documentType.get();
//    }
//
//    public void setDocumentType(String documentType) {
//        this.documentType.set(documentType);
//    }
//
//    public String getEmail() {
//        return email.get();
//    }
//
//    public void setEmail(String email) {
//        this.email.set(email);
//    }
//
//    public String getPhoneNumber() {
//        return phoneNumber.get();
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber.set(phoneNumber);
//    }
//
//    public String getStreet() {
//        return street.get();
//    }
//
//    public void setStreet(String street) {
//        this.street.set(street);
//    }
//
//    public String getCity() {
//        return city.get();
//    }
//
//    public void setCity(String city) {
//        this.city.set(city);
//    }
    
    /**
     * 
     * Właściwość 
     * @param event 
     */
   
    @FXML
    void onWRentActionButton(ActionEvent event) 
    {

    }
    @FXML
    void onWActuallyBorrowedBooksAction(ActionEvent event)
    {
        
    }
    @FXML
    void onSearchReaderAction(ActionEvent event)
    {
        ReaderBorrow rb;
        
        if((rb = SQLController.getInstance().getReaderToBorrow(Integer.valueOf(WReaderID.getText())))==null)
        {
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.setTitle("Nie znaleziono użytkownika!");
            alert.setHeaderText("Nie znaleziono użytkownika dla podanego ID."
                    + "Wprowadź inne id i spróbuj ponownie.");
            alert.showAndWait();
        }
        else
            setReader(rb);
    }
    @FXML
    void onWSearchForReaderAction(ActionEvent event)
    {
        
    }
    @FXML
    void onSearchBookAction(ActionEvent event)
    {
        
    }
    @FXML
    void onSearchBookActionButton(ActionEvent event)
    {
        
    }
    @FXML
    void initialize()
    {
        //ustawienie Properties dla TextField
//        WReaderID.textProperty().bindBidirectional(id,new NumberStringConverter());
//        WName.textProperty().bindBidirectional(name);
//        id.addListener((ObservableValue<? extends Number> observable, Number oldValue, Number newValue) -> {
//            System.out.println("Nowa wartość:"+newValue);
//            WReaderID.setText(String.valueOf(newValue));
//        });
        WSurname.setText("Kuchciacki");
    }
    public void setReader(ReaderBorrow rb)
    {
        logger.info("Ustawianie danych dla czytelnika");
        WSurname.setText(rb.getSurname());
        WName.setText(rb.getName());
        WReaderID.setText(String.valueOf(rb.getId()));
        PeselValidator pv = new PeselValidator(rb.getPESEL());
        WBirthDate.setText(pv.getBirthDayDate());
        WDocumentID.setText(rb.getDocumentNumber());
        WDocumentType.setText(rb.getDocumentType());
        WPESEL.setText(rb.getPESEL());
        WMail.setText(rb.getEmail());
        WPhoneNumber.setText(rb.getPhoneNumber());
        WStreet.setText(rb.getStreet());
        WCity.setText(rb.getCity());
        logger.info("Ustawiono dane użytkownika");
    }
    public void setBook(BookBorrow bb)
    {
        WBookID.setText(String.valueOf(bb.getID()));
        WISBN.setText(bb.getISBN());
        WTitle.setText(bb.getTitle());
        WAuthor.setText(bb.getAuthor());
        WYear.setText(bb.getYear());
        WKind.setText(bb.getKind());
        WCondition.setText(bb.getCondition());
    }
}
