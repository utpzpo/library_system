/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.controllers;

import com.library.entities.Book;
import com.library.entities.BookBorrow;
import com.library.sql.SQLController;
import java.io.IOException;
import java.util.ArrayList;
import javafx.beans.property.ListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author kucht
 */
public class BookController {
     /**
     * Logger dla klasy BookController.
     */
    public static Logger logger = LoggerFactory.getLogger(MainController.class);
    @FXML
    private BorrowController borrowController;
    public void setBorrowController(BorrowController borrowController)
    {
        this.borrowController=borrowController;
    }
    @FXML 
    ReturnController returnController;
    public void setReturnController(ReturnController returnController)
    {
        this.returnController=returnController;
    }
    @FXML
    ComunicationController comunicationController;
    public void setComunicationController(ComunicationController comunicationController)
    {
        this.comunicationController=comunicationController;
    }
    /**
     * fx:id="KNrInw"
     * Pole tekstowe w wyszukiwarce, w które należy wpisać nr inwentarzowy
     * książki.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KNrInw;
    /**
     * fx:id="KISBN"
     * Pole tekstowe w wyszukiwarce, w które należy wpisać nr ISBN książki.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KISBN;
    /**
     * fx:id="KAuthor"
     * Pole tekstowe w wyszukiwarce, w które należy wpisać Autora książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KAuthor;
    /**
     * fx:id="KTitle"
     * Pole tekstowe w wyszukiwarce, w które należy wpisać Tytuł książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    private TextField KTitle;
    /**
     * fx:id="KKey"
     * Pole tekstowe w wyszukiwarce.
     * Jest to pole uniwersalne, przeszukujące słowa kluczowe w bazie.
     * Value injected by FXMLLoader
     */
    @FXML
    private TextField KKey;
    /**
     * fx:id="KKind"
     * Lista wyboru Kategorii.
     * Predefiniowana lista w bazie.
     * @see Categories
     * Value injected by FXMLLoader
     */
    @FXML
    private ComboBox<String> KKind;
    /**
     * fx:id="KSearch
     * Przycisk wywołujący funkcję szukania.
     * @see onKSearchActionButton
     * Value injected by FXMLLoader
     */
    @FXML
    private Button KSearch;
    /**
     * fx:id="KClear"
     * Przycisk wywołujący funkcję czyszczenia pól w wyszukiwarce.
     * @see onKClearActionButton
     * Value injected by FXMLLoader
     */
    @FXML
    private Button KClear;
    /**
     * fx:id="KTable"
     * Tabela, w której pojawiają się wszystkie książki dostępne w bazie.
     * Widok w tabeli jest aktualizowany, kiedy zostanie wywołana wyszukiwarka.
     * Value injected by FXMLLoader
     */
    @FXML 
    public TableView<Book> KTable;
    /**
     * fx:id="KAdd"
     * Przycisk wywołujący nowe okno z dodawaniem nowej książki do bazy danych.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button KAdd;
    /**
     * fx:id="KEdit"
     * Przycisk wywołujący nowe okno z możliwością edycji zaznaczonej książki.
     * Value injected by FXMLLoader
     */
    @FXML 
    private Button KEdit;
    /**
     * fx:id="KReturn"
     * Przycisk przekazujący dane zaznaczonej książki do zakładki Zwroty.
     * W przypadku braku zaznaczenia, wyświetla odpowiedni komunikat.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button KReturn;
    /**
     * fx:id="KRent"
     * Przycisk przekazujący dane zaznaczonej książki do zakładki Wypożyczenia.
     * W przypadku braku zaznaczenia, wyświetla odpowiedni komunikat.
     * Value injected by FXMLLoader
     */
    @FXML
    private Button KRent;
    /**
     * Grid Pane z polami do wyszukiwania.
     */
    @FXML
    private GridPane KFieldsPane;
    
    private ObservableList<Book> bookList;
    
    private ListProperty<Book> bookProperty;
    
    private ArrayList<Book> books;
    
    @FXML
    void onKAddActionButton(ActionEvent event) {
        try {
            new AddBookController();
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }
    }

    @FXML
    void onKClearActionButton(ActionEvent event) {
        KFieldsPane.getChildren().stream() //tworzenie strumienia z listy
                .filter(node -> (node instanceof TextField)) //sprawdzanie, czy obiekt jest instancją TextField
                .forEach((node) -> {
                    ((TextField) node).clear(); //dla każdego takiego pola czyści się zawartość.
                });
        
    }

    @FXML
    void onKEditActionButton(ActionEvent event) {
        try{
            new EditBookController();
        }
        catch(IOException ex)
        {
            logger.error(ex.getMessage());
        }
    }

    @FXML
    void onKISBNActionButton(ActionEvent event) {
        onKSearchActionButton(event);
    }

    @FXML
    void onKRentActionButton(ActionEvent event) {
        BookBorrow bb = SQLController.getInstance().getBookToBorrow(
        KTable.getSelectionModel().getSelectedItem().getID());
        borrowController.setBook(bb);
    }

    @FXML
    void onKReturnActionButton(ActionEvent event) {
        BookBorrow bb = SQLController.getInstance().getBookToBorrow(
        KTable.getSelectionModel().getSelectedItem().getID());
        returnController.setBook(bb);
    }

    @FXML
    void onKSearchActionButton(ActionEvent event) {
        books=SQLController.getInstance().getBookWithSearchDetails(
                KNrInw.getText().length()>0?Integer.valueOf(KNrInw.getText()):0,
                KKind.getSelectionModel().getSelectedItem()!=null?
                        KKind.getSelectionModel().getSelectedItem():"NULL",
                KISBN.getText().length()>0?KISBN.getText():"NULL",
                KAuthor.getText().length()>0?KAuthor.getText():"NULL",
                KTitle.getText().length()>0?KTitle.getText():"NULL",
                KKey.getText().length()>0?KKey.getText():"NULL");
        bookList.clear();
        bookList.addAll(books);
               
    }
    @FXML
    public void initialize()
    {
        books = SQLController.getInstance().getAllBooks();
        bookList = FXCollections.observableArrayList(books);
        //Tworzenie kolumn dla tabeli
        TableColumn id = new TableColumn("ID");
        id.setCellValueFactory(new PropertyValueFactory<>("ID"));
        TableColumn ISBN = new TableColumn("ISBN");
        ISBN.setCellValueFactory(new PropertyValueFactory<>("ISBN"));
        ISBN.setSortable(true);
        TableColumn author = new TableColumn("Autor");
        author.setCellValueFactory(new PropertyValueFactory<>("author"));
        TableColumn title = new TableColumn("Tytuł");
        title.setCellValueFactory(new PropertyValueFactory<>("title"));
        TableColumn isAvailable = new TableColumn("Czy dostępna");
        isAvailable.setCellValueFactory(new PropertyValueFactory<>("isAvailable"));
        KTable.setEditable(false);
        KTable.setItems(bookList);
        KTable.getColumns().addAll(id,ISBN,author,title,isAvailable);
        KTable.setOnMouseClicked((MouseEvent event) ->{
            if(KTable.getSelectionModel().getSelectedItem()!=null)
            {
                
            }
        });
        KKind.setItems(FXCollections.observableArrayList(
        SQLController.getInstance().getAllKinds()));
        
        
    }
}
