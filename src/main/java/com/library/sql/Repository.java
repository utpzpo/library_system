/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.sql;

import com.library.entities.Book;
import com.library.entities.BookBorrow;
import com.library.entities.BooksItem;
import com.library.entities.Reader;
import com.library.entities.ReaderBorrow;
import com.library.entities.User;
import java.util.ArrayList;

/**
 *
 * @author kucht
 */

public interface Repository {
    
    public void addUser(String login, String password);
        
    public void addReader(String name, String surname, String PESEL, 
            String documentType, String documentNumber, String email, 
            String phoneNumber, String street, String city);
    
    public int getLastIndexOfReader();
    
    public ArrayList<User> getUsers();
    
    public boolean userLogon(String login, String password);
    
    public Reader getReader(int ID);
    
    public ArrayList<Reader> getAllReaders();
    
    public ArrayList<Book> getAllBooks();
    
    public BookBorrow getBookToBorrow(int id);
    
    public ReaderBorrow getReaderToBorrow(int id);
    
    public String getBorrowedBookDate(int bookID);
    
    public ArrayList<Reader> getReaderWithSearchDetails(int id, 
            String PESEL, String name, String surname);
    
    public ArrayList<Book> getBookWithSearchDetails(int id, String kind, String ISBN, String author, String title, String keywords);
    
    public ArrayList<BooksItem> getOverduedBooks(int ID);
    
    public ArrayList<BooksItem> getActiveRentedBooks(int id);
    
    public void addBook(int id, String isbn, String author, String title,
            String year, String condition, String kind, String status);
    public void archiveCommunicate(int id, String content);
    
    public ArrayList<String> getAllKinds();
    
    public ArrayList<String> getAllConditions();
    
    public void editBook(int id, String isbn, String author, String title,
            String year, String condition, String kind, String status);
    public ArrayList<BooksItem> getAllRentedBooksByReader(int id);
    
    public void saveDictionary(ArrayList<String> items, String dictionary);
    
    public ArrayList<String> getAllDocumentTypes();
}
