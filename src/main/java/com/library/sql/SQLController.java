/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.sql;

import com.library.entities.Book;
import com.library.entities.BookBorrow;
import com.library.entities.BooksItem;
import com.library.entities.Reader;
import com.library.entities.ReaderBorrow;
import com.library.entities.User;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.security.*;
import java.util.ArrayList;
import javafx.scene.control.Alert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Kontroler odpowiedzialny za połączenie bazy z modelem.
 * Klasa implementuje wzorzec projektowy Singleton.
 * @author kucht
 */
public class SQLController implements Repository{
     /**
     * Logger dla klasy SQLController.
     * Loguje wszelkie zapytania do bazy danych.
     */
    public static Logger logger = LoggerFactory.getLogger(SQLController.class);
    private static SQLController instance;
    /**
     * URL dla bazy danych.
     */
    public String URL="src/main/resources/database/database.db";
    
    public static SQLController getInstance()
    {
        if(instance==null)
            instance = new SQLController();
        return instance;
    }
    /**
     * Funkcja jest odpowiedzialna za zahashowanie w formacie MD5.
     * @param text - hasło przekazane do hashowania
     * @return zahashowane hasło
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException 
     */
    public static String hashMD5(String text) 
            throws NoSuchAlgorithmException, UnsupportedEncodingException
    {  
        byte[] bytesOfMessage = text.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        StringBuffer sb = new StringBuffer();
        byte[] theDigest = md.digest(bytesOfMessage);
        for(byte i : theDigest)
        {
            sb.append(String.format("%02x", i & 0xff));
        }
        return sb.toString();
    }
    /**
     * Funkcja odpowiedzialna jest za połączenie się z bazą SQLite.
     * @return Zwraca referencję do połączenia z bazą danych. 
     */    
    public Connection connect() 
    {
        Connection conn = null;
        try
        {
        Class.forName("org.sqlite.JDBC");
        File f = new File(URL);
        String url = "jdbc:sqlite:"+f.getAbsolutePath();
        conn = DriverManager.getConnection(url);
            logger.info("Połączenie z bazą nawiązane !");
        }
        catch(SQLException | ClassNotFoundException ex)
        {
            logger.error(ex.getMessage());
        }
        return conn;
    }
    /**
     * Zamyka połączenie z bazą danych.
     * @param conn - referencja do połączenia z bazą
     */    
    public void disconnect(Connection conn)
    {
            try
            {
                if(conn!=null)
                    conn.close();
            }
            catch(SQLException s)
            {
                logger.error(s.getMessage());
            }
        
    }
    /**
     * Dodaje użytkownika do bazy danych. Sprawdza, czy użytkownik nie istnieje już w bazie
     * @param login - login 
     * @param password - hasło
     */    
    @Override
    public void addUser(String login, String password) 
    {
        Connection conn = null;
        try {
            conn = connect();
            PreparedStatement ps = conn.prepareStatement
                ("SELECT * FROM USERS WHERE LOGIN = ?");
            ps.setString(1, login);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            if(rs.wasNull())
            {
                ps=conn.prepareStatement
                    ("INSERT INTO USERS (login,password) VALUES(?,?)");
                ps.setString(1, login);
                try
                {
                    password=hashMD5(password);
                    ps.setString(2, password);
                    boolean success=ps.execute();
                    if(success)
                    {
                        logger.info("Poprawnie dodany użytkownik");
                    }
                    else
                    {
                        logger.error("Nieudało się dodać użytkownika! ");
                    }
                }
                catch(NoSuchAlgorithmException | UnsupportedEncodingException  ex)
                {
                    logger.error(ex.getMessage());
                }
                catch(SQLException ex)
                {
                    logger.error(ex.getSQLState());
                }
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
    }
    /**
     * Funkcja dodaje czytelnika do bazy danych
     * @param name - imię
     * @param surname - nazwisko
     * @param PESEL
     * @param documentType - typ dokumentu
     * @param documentNumber - numer dokumentu
     * @param email - email
     * @param phoneNumber - numer telefonu
     * @param street - ulica
     * @param city - miejscowość
     */
    @Override
    public void addReader(String name, String surname, String PESEL, 
            String documentType, String documentNumber, String email, 
            String phoneNumber, String street, String city) 
            
    {
        Connection conn=null;
        try
        {
        conn = connect();
        PreparedStatement ps = conn.prepareStatement(
        "INSERT INTO READER (Name, Surname, PESEL, Document_type, Document_ID"
                + "Email, Phone_number, Street, City) VALUES(?,?,?,?,?,?,?,?,?");
        ps.setString(1, name);
        ps.setString(2, surname);
        ps.setString(3, PESEL);
        ps.setString(4, documentType);
        ps.setString(5, documentNumber);
        ps.setString(6, email);
        ps.setString(7, phoneNumber);
        ps.setString(8, street);
        ps.setString(9, city);
            boolean success = ps.execute();
            System.out.println("Zapytanie");
            if(success)
            {
                logger.info("Udało się dodać czytelnika do bazy !");
            }
            else
            {
                logger.error("Nieudało się dodać czytelnika do bazy !");
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getSQLState());
        }
        finally
        {
            disconnect(conn);
        }
    }
    /**
     * Pobiera indeks ostatniego użytkownika w bazie.
     * @return index ostatniego czytelnika
     */
    @Override
    public int getLastIndexOfReader()
    {
        int index=0;
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Reader ORDER BY ID DESC LIMIT 1;");
            ResultSet rs = ps.executeQuery();
            if(!rs.wasNull())
            {
                index=rs.getInt(1);
            }
        }
        catch(SQLException sqle)
        {
            logger.error(sqle.getSQLState());
        }
        finally
        {
            disconnect(conn);
        }
        return index+1;
    }
    /**
     * Pobiera wszystkich użytkowników z bazy.
     * @return Lista użytkowników w bazie.
     */
    @Override
    public ArrayList<User> getUsers() {
        Connection conn = null;
        ArrayList list = new ArrayList<>();
        try
        {
            conn = connect();
            PreparedStatement ps = conn.prepareStatement("Select Login from Users");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                list.add(rs.getString(1));
            }
        } catch (SQLException ex) {
            logger.error(ex.getSQLState());
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return list;
        
    }
    /**
     * Symuluje logowanie użytkownika do aplikacji.
     * @param login - login użytkownika
     * @param password - hasło użytkownika
     * @return - zwraca czy logowanie się powiodło czy nie
     */
    @Override
    public boolean userLogon(String login, String password) {
        Connection conn = null;
        try {
            String pass=hashMD5(password);
        } catch (NoSuchAlgorithmException ex) {
            logger.error(ex.getMessage());
        } catch (UnsupportedEncodingException ex) {
           logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return true;
    }

    @Override
    public Reader getReader(int ID) {
        Connection conn = null;
        Reader reader = null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM READER_ENTITY WHERE ID=?");
            ps.setInt(1, ID);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                reader = new Reader(rs.getInt(1),rs.getString(2),
                        rs.getString(3),rs.getString(4),rs.getString(5));
            }
        } catch (SQLException ex) {
            logger.error(ex.toString());
        }
        return reader;
    }

    @Override
    public ArrayList<Reader> getAllReaders() {
        ArrayList list = new ArrayList<>();
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select * from Reader_Entity");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                Reader reader = new Reader();
                reader.setId(rs.getInt(1));
                reader.setName(rs.getString(2));
                reader.setSurname(rs.getString(3));
                reader.setPESEL(rs.getString(4));
                reader.setNumberOfRentedBooks(String.valueOf(rs.getInt(5)));
                list.add(reader);
            }
        } catch (SQLException ex) {
                logger.error(ex.getMessage());
        }
        return list;
    }
    @Override
    public ArrayList<Book> getAllBooks()
    {
        ArrayList<Book> books = new ArrayList();
        Connection conn = null;
        try
        {
            conn = connect();
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM Book_Entity");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                Book book = new Book(rs.getInt(1),rs.getString(2),rs.getString(3),
                rs.getString(4),rs.getInt(5));
                books.add(book);
            }
        } catch (SQLException ex) {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return books;
    }
    
    @Override
    public BookBorrow getBookToBorrow(int id)
    {
        BookBorrow bb=null;
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select * from BookToBorrow where id=?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                bb = new BookBorrow(rs.getInt(1),rs.getString(2),rs.getString(3),
                rs.getString(4),rs.getString(5),rs.getString(6),rs.getString(7));
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return bb;
    }
    
    @Override
    public ReaderBorrow getReaderToBorrow(int id)
    {
        ReaderBorrow rb=null;
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select * from ReaderToBorrow "
                    + "where id = ?");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                rb=new ReaderBorrow(id, rs.getString(2), rs.getString(3), 
                        rs.getString(4), rs.getString(5), rs.getString(6),
                        rs.getString(7), rs.getString(8), rs.getString(9),
                        rs.getString(10));
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        
        return rb;
    }
    
    @Override
    public String getBorrowedBookDate(int bookID)
    {
        Connection conn=null;
        String date="";
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select start_date from Borrows"
                    + " where Book_ID = ? and End_date=NULL");
            ps.setInt(1, bookID);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                date=rs.getString(1);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return date;
    }

    @Override
    public ArrayList<Book> getBookWithSearchDetails(int id, String kind, String ISBN, String author, String title, String keywords) {
        ArrayList<Book> books = new ArrayList();
        Connection conn = null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement(
                    "Select * from Book_Entity where "
                    + "id like ? "
                    + "or"
                    + " kind like ? "
                    + "or "
                    + "ISBN like ? "
                    + "or "
                    + "author like ?"
                    + " or"
                    + " title like ? "
                    + "or "
                    + "author like ? "
                    + "or "
                    + "title like ?;");
            ps.setInt(1, id);
            ps.setString(2, "%"+kind+"%");
            ps.setString(3, "%"+ISBN+"%");
            ps.setString(4, "%"+author+"%");
            ps.setString(5, "%"+title+"%");
            ps.setString(6, "%"+keywords+"%");
            ps.setString(7, "%"+keywords+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                Book book = new Book(rs.getInt(1),rs.getString(2),rs.getString(3),
                rs.getString(4),rs.getInt(5));
                books.add(book);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return books;
    }

    @Override
    public ArrayList<Reader> getReaderWithSearchDetails(int id, String PESEL, String name, String surname) {
        System.out.println("Wartość id: "+id);
        ArrayList<Reader> readers = new ArrayList<>();
        Connection conn = null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement(
                "Select * from Reader_Entity "
                        + "where "
                        + "id = ? "
                        + "or PESEL like ? "
                        + "or name like ? "
                        + "or surname like ?;");
            ps.setInt(1, id);
            ps.setString(2, "%"+PESEL+"%");
            ps.setString(3, "%"+name+"%");
            ps.setString(4, "%"+surname+"%");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                Reader reader = new Reader(rs.getInt(1),rs.getString(2),
                        rs.getString(3),rs.getString(4),rs.getString(5));
                readers.add(reader);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
            logger.error(ex.getSQLState());
        }
        finally
        {
            disconnect(conn);
        }
        return readers;
    }
    /**
     * Pobiera lista książek, których termin oddania już minął.
     * @param id - id czytelnika
     * @return lista książek, których termin oddania już minął.
     */
    @Override
    public ArrayList<BooksItem> getOverduedBooks(int id)
    {
        ArrayList<BooksItem> overduedList = new ArrayList<>();
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement(
                "Select borrows.Book_ID, ISBN.Title "
                + "from (Book join ISBN on book.ISBN=isbn.ISBN)"
                + " join borrows on Book.ID=borrows.Book_ID where "
                + "(select date(Start_date, '+10 days')) > (select date('now'))"
                + " and Reader_ID = ?;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                BooksItem ob = new BooksItem(rs.getInt(1),rs.getString(2),rs.getString(3));
                overduedList.add(ob);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return overduedList;
    }

    @Override
    public ArrayList<BooksItem> getActiveRentedBooks(int id) {
        ArrayList<BooksItem> booksItems = new ArrayList<>();
        Connection conn = null;
        try
        {
            conn = connect();
            PreparedStatement ps = conn.prepareStatement("Select borrows.Book_ID, ISBN.Title, borrows.End_date "
                + "from (Book join ISBN on book.ISBN=isbn.ISBN)"
                + " join borrows on Book.ID=borrows.Book_ID where "
                + "End_date is NULL"
                + " and Reader_ID = ?;");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                BooksItem item = new BooksItem(rs.getInt(1),rs.getString(2),rs.getString(3).isEmpty()?"":rs.getString(3));
                booksItems.add(item);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
            ex.printStackTrace();
  
            logger.error(ex.getSQLState());
        }
        finally
        {
            disconnect(conn);
        }
        return booksItems;
    }

    @Override
    public void addBook(int id, String isbn, String author, String title, String year, String condition, String kind, String status) {
        Connection conn= null;
        try
        {
            conn=connect();
            conn.setAutoCommit(false);
            
            PreparedStatement ps = conn.prepareStatement("select * from isbn where isbn=?");
            ps.setString(1, isbn);
            ResultSet rs = ps.executeQuery();
            if(rs.wasNull())
            {
                ps=conn.prepareStatement("Insert into isbn values(?,?,?,?,?,?);");
                ps.setString(1, isbn);
                ps.setString(2, author);
                ps.setString(3, title);
                ps.setString(4, year);
                ps.setString(5, kind);
                ps.setString(6, "(select count(*)+1 from book where isbn='"+isbn+"')");
                ps.execute();
            }
            ps=conn.prepareStatement("Insert into book values(?,?,?,?");
            ps.setInt(1, id);
            ps.setString(2, isbn);
            ps.setString(3, URL);
            ps.setString(4, status);
            if(ps.execute())
            {
            ps=conn.prepareStatement("Update isbn set Quantity = ("
                    + "Select Count(*)+1 from book where isbn=?);");
            ps.setString(1, isbn);
            int i=ps.executeUpdate();
            if(i>0)
                conn.commit();
            logger.info("Sukces, udało się dodać książkę do bazy");
            }
            else
            {
                conn.rollback();
                logger.warn("Coś poszło nie tak. Nie zapisano nic do bazy.");
                Alert alert = new Alert(Alert.AlertType.WARNING);
                alert.setTitle("Nie zapisano do bazy");
                alert.setAlertType(Alert.AlertType.WARNING);
                alert.setHeaderText("Nie udało się zapisać danych do bazy");
                alert.showAndWait();
            }
            
        }
        catch(SQLException ex )
        {
            logger.error(ex.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Nie zapisano do bazy");
            alert.setAlertType(Alert.AlertType.ERROR);
            alert.setHeaderText("Błąd: "+ex.getMessage());
            alert.showAndWait();
        }
    }
    @Override
    public void archiveCommunicate(int id, String content)
    {
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Insert into"
                    + " Archive_com values(?,?);");
            ps.setInt(1, id);
            ps.setString(2, content);
            if(ps.execute())
            {
                logger.info("Udane zarchiwizowanie komunikatu");
                
            }
        }
        catch(SQLException ex)
        {
            logger.error("Nie udało się zarchiwizować komunikatu! "+ex.getMessage());
        }
    }

    @Override
    public ArrayList<String> getAllConditions() {
        ArrayList<String> conditions = new ArrayList<>();
        Connection conn = null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select * from Condition_enum;");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                String condition = rs.getString(1);
                conditions.add(condition);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return conditions;
    }

    @Override
    public ArrayList<String> getAllKinds() {
        ArrayList<String> kinds = new ArrayList<>();
        Connection conn=null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select * from KindOfBook_enum;");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                String kind = rs.getString(1);
                kinds.add(kind);
            }
            
        }
        catch(SQLException ex )
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return kinds;
    }

    @Override
    public void editBook(int id, String isbn, String author, String title, String year, String condition, String kind, String status) {
        Connection conn = null;
        try
        {
            conn=connect();
            conn.setAutoCommit(false);
            PreparedStatement ps = conn.prepareStatement(
            "Update book "
                    + "set ISBN=?,author=?,title=?,year=?,condition=?,kind=?,status=? "
                    + "where id=?;");
            ps.setString(1, isbn);
            ps.setString(2, author);
            ps.setString(3, title);
            ps.setString(4, year);
            ps.setString(5, condition);
            ps.setString(6, kind);
            ps.setString(7, status);
            ps.setInt(8, id);
            if(ps.executeUpdate()==1)
            {
                logger.info("Sukces, zaktualizowano książkę o id="+id);
                conn.commit();
            }
            else
            {
                logger.warn("Nie udało się zaktualizować książki");
            }
            
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
    }
    
    @Override
    public ArrayList<BooksItem> getAllRentedBooksByReader(int id)
    {
        ArrayList<BooksItem> booksItems = new ArrayList();
        Connection conn = null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement(
                "Select borrows.Book_ID, ISBN.Title, borrows.End_date "
                + "from (Book join ISBN on book.ISBN=isbn.ISBN)"
                + " join borrows on Book.ID=borrows.Book_ID where Reader_ID = ?;");
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                BooksItem item = new BooksItem(rs.getInt(1),rs.getString(2),
                rs.getString(3).isEmpty()?"":rs.getString(3));
                booksItems.add(item);
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return booksItems;
    }
    
    @Override
    public void saveDictionary(ArrayList<String> items, String dictionary)
    {
        Connection conn = null;
        try
        {
            conn=connect();
            PreparedStatement ps;
            for(String item:items)
            {
                ps=conn.prepareStatement("Insert into ? values(?);");
                ps.setString(1, dictionary);
                ps.setString(2, item);
                ps.execute();
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
    }
    
    @Override
    public ArrayList<String> getAllDocumentTypes()
    {
        ArrayList<String> types = new ArrayList<>();
        Connection conn = null;
        try
        {
            conn=connect();
            PreparedStatement ps = conn.prepareStatement("Select * from Document_type;");
            ResultSet rs = ps.executeQuery();
            while(rs.next())
            {
                types.add(rs.getString(1));
            }
        }
        catch(SQLException ex)
        {
            logger.error(ex.getMessage());
        }
        finally
        {
            disconnect(conn);
        }
        return types;
    }
}
