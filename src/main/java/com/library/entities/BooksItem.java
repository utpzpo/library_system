/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entities;

import java.util.Date;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Encja, która odzwierciedla przeterminowaną książkę.
 * Potrzebne jest jej tylko id i tytuł.
 * @author kucht
 */
public class BooksItem {
    
    /**
     * Id książki.
     */
    private IntegerProperty id= new SimpleIntegerProperty();
    /**
     * Tytuł książki.
     */
    private StringProperty title = new SimpleStringProperty();
    /**
     * Data oddania książki.
     */
    private StringProperty date = new SimpleStringProperty();
    /**
     * Getter for id.
     * @return id
     */
    public int getId() {
        return id.get();
    }
    /**
     * Setter for id
     * @param id id książki.
     */
    public void setId(int id) {
        this.id.set(id);
    }
    /**
     * Getter for title.
     * @return title.
     */
    public String getTitle() {
        return title.get();
    }
    /**
     * Setter for title.
     * @param title - tytuł książki.
     */
    public void setTitle(String title) {
        this.title.set(title);
    }

    public String getDate() {
        return date.get();
    }

    public void setDate(String date) {
        this.date.set(date);
    }
    @Override
    public String toString() {
        return "Id=" + id + " "+title + '}';
    }
    /**
     * Konstruktor dla wszystkich pól
     * @param id - id
     * @param title - tytuł
     */
    public BooksItem(int id, String title, String date) {
        this.id.set(id);
        this.title.set(title);
        this.date.set(date);
    }
    /**
     * Konstruktor domyślny.
     */
    public BooksItem() {
    }
    
    
    public IntegerProperty bookIdProperty(){return id;}
    
    public StringProperty bookTitleProperty(){return title;}
    
    public StringProperty bookDateProperty(){return date;}
    
}
