/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entities;

import javafx.beans.property.StringProperty;

/**
 *
 * @author kucht
 */
public class User {
    /**
     * Login użytkownika.
     */
    private StringProperty login;
    /**
     * Getter dla pola login.
     * @return login usera.
     */
    public StringProperty getLogin() {
        return login;
    }
    /**
     * Setter dla pola login
     * @param login - login usera
     */
    public void setLogin(StringProperty login) {
        this.login = login;
    }
    /**
     * Konstruktor encji User.
     * @param login - login użytkownika.
     */
    public User(String login) {
        this.login.set(login);
    }
    /**
     * Nadpisana metoda toString()
     * @return zwraca login z właściwośći
     */
    @Override
    public String toString() {
        return login.get();
    }
    
    
}
