/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Encja na potrzeby tabeli w zakładce czytelnicy.
 * @author kucht
 */

public class Reader {
    
    /**
     * Imię czytelnika.
     * Właściwość klasy Reader.
     */
    private StringProperty name = new SimpleStringProperty();
    /**
     * Nazwisko czytelnika.
     * Właściwość klasy Reader.
     */
    private StringProperty surname= new SimpleStringProperty();
    /**
     * ID czytelnika.
     * Właściwość klasy Reader.
     */
    private IntegerProperty id = new SimpleIntegerProperty();
    /**
     * PESEL czytelnika.
     */
    private StringProperty PESEL =  new SimpleStringProperty();
    /**
     * Ilość aktualnie wypożyczonych książek.
     * Właściwość klasy Reader.
     */
    private StringProperty numberOfRentedBooks = new SimpleStringProperty();
    
    public IntegerProperty readerIdProperty(){ return id;}
    
    public StringProperty readerNameProperty(){return name;}
    
    public StringProperty readerSurnameProperty(){return surname;}
    
    public StringProperty readerPESELProperty(){return PESEL;}
    
    public StringProperty readerNumberOfRentedBooks(){return numberOfRentedBooks;}
    /**
     * Pobiera imię danego czytelnika.
     * @return imię czytelnika 
     */
    public String getName() {
        return name.get();
    }
    /**
     * Ustawia imię czytelnika.
     * @param name - imię
     */
    public void setName(String name) {
        this.name.set(name);
    }
    /**
     * Pobiera nazwisko danego czytelnika.
     * @return surname - nazwisko czytelnika.
     */
    public String getSurname() {
        return surname.get();
    }
    /**
     * Ustawia nazwisko danego czytelnika.
     * @param surname - nazwisko czytelnika
     */
    public void setSurname(String surname) {
        this.surname.set(surname);
    }
    /**
     * Zwraca id danego czytelnika.
     * @return id - numer czytelnika
     */
    public int getId() {
        return id.get();
    }
    /**
     * Ustawia nr id danego czytelnika.
     * @param id - nr czytelnika.
     */
    public void setId(int id) {
        this.id.set(id);
    }
    /**
     * Zwraca PESEL dla danego czytelnika.
     * @return PESEL czytelnika
     */
    public String getPESEL() {
        return PESEL.get();
    }
    /**
     * Ustawia PESEL dla danego czytelnika.
     * @param PESEL - PESEL czytelnika
     */
    public void setPESEL(String PESEL) {
        this.PESEL.set(PESEL);
    }
    /**
     * Zwraca ilość aktualnie wypożyczonych książek.
     * @return ilość aktualnie wypożyczonych książek.
     */
    public String getNumberOfRentedBooks() {
        return numberOfRentedBooks.get();
    }
    /**
     * Ustawia ilość aktualnie wypożyczonych książek.
     * @param numberOfRentedBooks  - ilość aktualnie wypożyczonych książek.
     */
    public void setNumberOfRentedBooks(String numberOfRentedBooks) {
        this.numberOfRentedBooks.set(numberOfRentedBooks);
    }
    
    public Reader(int id, String name, String surname, String PESEL, String number)
    {
        this.id.set(id);
        this.name.set(name);
        this.surname.set(surname);
        this.PESEL.set(PESEL);
        this.numberOfRentedBooks.set(number);
    }
    
    public Reader()
    {
        
    }
}
