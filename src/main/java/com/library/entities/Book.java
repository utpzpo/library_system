/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entities;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author kucht
 */
public class Book {
    /**
     * Pole ID - numer książki w bibliotece.
     */
    private IntegerProperty ID = new SimpleIntegerProperty();
    /**
     * Pole ISBN - numer ISBN książki
     */
    private StringProperty ISBN = new SimpleStringProperty();
    /**
     * Pole author - autor książki.
     */
    private StringProperty author = new SimpleStringProperty();
    /**
     * Pole title - tytuł książki.
     */
    private StringProperty title = new SimpleStringProperty();
    /**
     * Pole isAvailable - informuje ile książek jest dostępnych.
     */
    private StringProperty isAvailable = new SimpleStringProperty();
    /**
     * @return ID ksiązki
     */
    public int getID() {
        return ID.get();
    }
    /**
     * 
     * @param ID ksiązki
     */
    public void setID(int ID) {
        this.ID.set(ID);
    }
    /**
     * 
     * @return ISBN książki
     */
    public String getISBN() {
        return ISBN.get();
    }
    /**
     * 
     * @param ISBN książki
     */
    public void setISBN(String ISBN) {
        this.ISBN.set(ISBN);
    }
    /**
     * 
     * @return autor ksiązki 
     */
    public String getAuthor() {
        return author.get();
    }
    /**
     * 
     * @param author ksiązki
     */
    public void setAuthor(String author) {
        this.author.set(author);
    }
    /**
     * 
     * @return tytuł książki
     */
    public String getTitle() {
        return title.get();
    }
    /**
     * 
     * @param title - tytuł ksiązki 
     */
    public void setTitle(String title) {
        this.title.set(title);
    }
/**
 * 
 * @return ile ksiązek jest dostępnych 
 */
    public String getIsAvailable() {
        return isAvailable.get();
    }
    /**
     * 
     * @param isAvailable - ile książek jest dostępnych 
     */
    public void setIsAvailable(String isAvailable) {
        this.isAvailable.set(isAvailable);
    }
    /**
     * Konstruktor dla klasy Book.
     * @param ID
     * @param ISBN
     * @param author
     * @param title
     * @param isAvailable 
     */
    public Book(int ID, String ISBN, String author, String title, int quantity) {
        this.ID.set(ID);
        this.ISBN.set(ISBN);
        this.author.set(author);
        this.title.set(title);
        if(quantity>0)
            this.isAvailable.set("Dostępny");
        else
            this.isAvailable.set("Niedostępny");
    }
}
