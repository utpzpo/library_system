/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.entities;

import com.library.verifier.PeselValidator;
import java.util.Date;

/**
 *
 * @author kucht
 */
public class ReaderBorrow {
    private int id;
    
    private String name;
    
    private String surname;
    
    private String dateOfBirth;
    
    private String documentNumber;
    
    private String documentType;
    
    private String PESEL;
    
    private String email;
    
    private String phoneNumber;
    
    private String street;
    
    private String city;

    public ReaderBorrow(int id, String name, String surname, String documentNumber, String documentType, String PESEL, String email, String phoneNumber, String street, String city) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.documentNumber = documentNumber;
        this.documentType = documentType;
        this.PESEL = PESEL;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.street = street;
        this.city = city;
        PeselValidator peselValidator = new PeselValidator(PESEL);
        this.dateOfBirth=new Date(peselValidator.getBirthYear(),
                peselValidator.getBirthMonth(), peselValidator.getBirthDay()).toString();
    }

    @Override
    public String toString() {
        return "ReaderBorrow{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", dateOfBirth=" + dateOfBirth + ", documentNumber=" + documentNumber + ", documentType=" + documentType + ", PESEL=" + PESEL + ", email=" + email + ", phoneNumber=" + phoneNumber + ", street=" + street + ", city=" + city + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getPESEL() {
        return PESEL;
    }

    public void setPESEL(String PESEL) {
        this.PESEL = PESEL;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
