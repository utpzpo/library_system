/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.reports;

import com.library.sql.SQLController;
import java.awt.Color;
import java.net.MalformedURLException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import net.sf.dynamicreports.report.builder.DynamicReports;
import static net.sf.dynamicreports.report.builder.DynamicReports.stl;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.builder.style.StyleBuilder;
import net.sf.dynamicreports.report.constant.HorizontalTextAlignment;
import net.sf.dynamicreports.report.constant.VerticalTextAlignment;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 *
 * @author kucht
 */
public class ReportGenerator {
    /**
     * Logger dla klasy ReportGenerator
     */
    public static Logger logger = LoggerFactory.getLogger(ReportGenerator.class);
    /**
     * Tworzy raport aktywnych wypożyczeń.
     * @throws ClassNotFoundException - JDBC nie zostało znalezione
     * @throws SQLException - błąd sql
     * @throws MalformedURLException - nieudane odnalezienie url
     */
    public static void buildActiveBorrowsReport() throws ClassNotFoundException, SQLException, MalformedURLException
    {
        Connection conn = SQLController.getInstance().connect();
        try {
            logger.info("Budowanie raportu Aktywnych wypożyczeń");
            StyleBuilder sty = stl.style().setFontName("Pictionic");
                       String query = "Select * from ActiveBorrows";
            StyleBuilder boldStyle = DynamicReports.stl.style().setFontName("Pictonic").bold();
            StyleBuilder boldCenteredStyle = DynamicReports.stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
            StyleBuilder columnTitleStyles = DynamicReports.stl.style(boldCenteredStyle).setBorder(DynamicReports.stl.pen1Point()).setBackgroundColor(Color.lightGray);
            StyleBuilder titleStyle = DynamicReports.stl.style(boldCenteredStyle).setVerticalTextAlignment(VerticalTextAlignment.MIDDLE).setFontSize(15);
            DynamicReports.report()
                    .columns(//dodawanie kolumn
                            //                  Tytuł       Nr ks           Typ danych
                            DynamicReports.col.column("Ksiązka", "Book_ID", DynamicReports.type.integerType()),
                            DynamicReports.col.column("Czytelnik", "Reader_ID", DynamicReports.type.integerType()),
                            DynamicReports.col.column("Tytuł", "Title", DynamicReports.type.stringType()),
                            DynamicReports.col.column("Autor", "Author", DynamicReports.type.stringType()),
                            DynamicReports.col.column("Data wypożyczenia", "Start_date", DynamicReports.type.stringType()))
//                            DynamicReports.col.column("Planowane oddanie", "End_date", DynamicReports.type.stringType()))
                    .title(DynamicReports.cmp.horizontalList().add(
                            DynamicReports.cmp.image("/src/main/resources/Icons/report_icon.png").setDimension(80, 80),
                            DynamicReports.cmp.text(("Aktywne wypożyczenia w bibliotece na dzień "+LocalDateTime.now().toString()))
                                    //.setStyle(titleStyle)
                                    .setHorizontalTextAlignment(HorizontalTextAlignment.RIGHT))
                            .newRow().add(
                                    DynamicReports.cmp.filler().setStyle(
                                            DynamicReports.stl.style().setTopBorder(
                                                    DynamicReports.stl.pen2Point())).setFixedHeight(10)))
                    .pageFooter(DynamicReports.cmp.pageXofY())
                    .setDataSource(query, conn)
                    //.setColumnTitleStyle(columnTitleStyles)
                    .highlightDetailEvenRows()
                    .show(false);
//            JasperReportBuilder report = DynamicReports.report();
//            report
//                    .columns(
//                    Columns.column("ID Czytelnika", "Reader_ID",DataTypes.integerType()),
//                    Columns.column("Imię","Name",DataTypes.stringType()),
//                    Columns.column("Nazwisko","Surname",DataTypes.stringType()),
//                    Columns.column("ID Książki","Book_ID",DataTypes.integerType()),
//                    Columns.column("Tytuł","Title",DataTypes.stringType()),
//                    Columns.column("Autor","Author",DataTypes.stringType()),
//                    Columns.column("Data wypożyczenia","Start_time",DataTypes.dateDayType()),
//                    Columns.column("Planowanie oddanie","End_time",DataTypes.dateDayType())
//            ).title(//tytuł raportu
//            Components.text("Raport aktywnych wypożyczeń")
//                        .setHorizontalTextAlignment(HorizontalTextAlignment.CENTER).setStyle(sty))
//                    .pageFooter(Components.pageXofY().setStyle(sty))
//                    .setDataSource(query, conn);
//                    
//            
//            report.toPdf(new FileOutputStream("d:/report.pdf"));
            logger.info("Sukces. Poprawnie zbudowano raport");
        } catch (Exception ex) {
            logger.error("Błąd w generowaniu raportu\n"+ex.getMessage());
        }
        finally
        {
            SQLController.getInstance().disconnect(conn);
        }
    }
    /**
     * Tworzy raport aktywnych dłużników.
     * @throws ClassNotFoundException - JDBC nie zostało znalezione
     * @throws SQLException - błąd sql
     * @throws MalformedURLException - nieudane odnalezienie url
     */
    public static void buildActiveBorrowersReport() throws SQLException, 
            MalformedURLException, ClassNotFoundException
    {
        Connection conn = SQLController.getInstance().connect();
        try {
            logger.info("Budowanie raportu Aktywnych dłużników");
            String query = "";
            StyleBuilder boldStyle = DynamicReports.stl.style().bold();
            StyleBuilder boldCenteredStyle = DynamicReports.stl.style(boldStyle);
            StyleBuilder columnTitleStyles = DynamicReports.stl.style
        (boldCenteredStyle).setBorder(DynamicReports.stl.pen1Point())
                    .setBackgroundColor(Color.lightGray);
            StyleBuilder titleStyle = DynamicReports.stl.
                    style(boldCenteredStyle).
                    setVerticalTextAlignment(VerticalTextAlignment.MIDDLE).
                    setFontSize(15);
            DynamicReports.report()
                    .columns(//dodawanie kolumn
                            //                           Tytuł       Pole           Typ danych
                            DynamicReports.col.column
        ("Czytelnik", "Reader_ID", DynamicReports.type.integerType()),
                            DynamicReports.col.column
        ("Imię", " Name", DynamicReports.type.stringType()),
                            DynamicReports.col.column
        ("Nazwisko", "Surname", DynamicReports.type.stringType()),
                            DynamicReports.col.column
        ("Kwota zadłużenia", "Mortgage", DynamicReports.type.bigDecimalType()))
                    .title(DynamicReports.cmp.horizontalList().add(
                            DynamicReports.cmp.image
        ("/src/main/resources/Icons/report_mortgage.png").setDimension(240, 160),
                            DynamicReports.cmp.text
        (("Aktywne wypożyczenia w bibliotece na dzień "+LocalDateTime.now().
                toString()))
                                    .setStyle(titleStyle).
                                    setHorizontalTextAlignment
        (HorizontalTextAlignment.RIGHT))
                            .newRow().add(
                                    DynamicReports.cmp.filler().setStyle(
                                            DynamicReports.stl.style()
                                                    .setTopBorder(
                                                    DynamicReports.stl
                                                            .pen2Point()))
                                            .setFixedHeight(10)))
                    .pageFooter(DynamicReports.cmp.pageXofY())
                    .setDataSource(query, conn)
                    .setColumnTitleStyle(columnTitleStyles)
                    .highlightDetailEvenRows()
                    .show(false);
            logger.info("Sukces. Poprawnie zbudowano raport");
        } catch (DRException ex) {
            logger.error("Błąd w generowaniu raportu\n"+ex.getMessage());
        }
        finally
        {
            SQLController.getInstance().disconnect(conn);
        }
    }
    /**
     * Funkcja generuje raport wypożyczeń dla każdego dnia.
     * @throws ClassNotFoundException - JDBC nie zostało znalezione
     * @throws SQLException - błąd sql
     * @throws MalformedURLException - nieudane odnalezienie url
     * @throws DRException - błąd przy tworzeniu wykresu przy użyciu 
     * Dynamic Reports
     */
    public static void borrowsPerDayOfMonthReport(String startDate, String endDate) throws MalformedURLException, SQLException, ClassNotFoundException, DRException
    {
        Connection conn = SQLController.getInstance().connect();
        String query="";
        StyleBuilder boldStyle = DynamicReports.stl.style().bold();
        StyleBuilder boldCenteredStyle = DynamicReports.stl.style(boldStyle).setHorizontalTextAlignment(HorizontalTextAlignment.CENTER);
        StyleBuilder titleStyle = DynamicReports.stl.style(boldCenteredStyle).setVerticalTextAlignment(VerticalTextAlignment.MIDDLE).setFontSize(15);
        FontBuilder boldFont = stl.fontArialBold().setFontSize(12);
        TextColumnBuilder<Integer> booksFromDay = DynamicReports.col.column("Ilość", "Count", DynamicReports.type.integerType());
        TextColumnBuilder<String> days = DynamicReports.col.column("Dzień", "Day", DynamicReports.type.stringType());
        DynamicReports.report()
                .title(DynamicReports.cmp.text("Wypożyczenia/dzień"))
                .summary(
                        DynamicReports.cht.bar3DChart()
                                        .setTitle("Wypożyczenia/dzień")
                                        .setTitleFont(boldFont)
                                        .setCategory(days)
                                        .series(
                                                DynamicReports.cht.serie(booksFromDay),
                                                DynamicReports.cht.serie(booksFromDay))
                                        .setCategoryAxisFormat(
                                                DynamicReports.cht.axisFormat().setLabel("ilość")))
                .pageFooter(DynamicReports.cmp.pageXofY())
                .setDataSource(query, conn)
                .show(false);
    }
   
    
}
