/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.library.sql;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author kucht
 */
public class TestSQLController {
    
    @Test
    public void testMethodConnect() throws MalformedURLException, SQLException, ClassNotFoundException {
       
        
          new SQLController().connect();
          Assert.assertTrue(true);
    }
    
    @Test
    public void testMethodDigest() throws UnsupportedEncodingException, NoSuchAlgorithmException
    {
                String password = "abcd";
                byte[] bytesOfMessage = password.getBytes("UTF-8");
                MessageDigest md = MessageDigest.getInstance("MD5");
                StringBuffer sb = new StringBuffer();
                byte[] theDigest = md.digest(bytesOfMessage);
                for(byte i : theDigest)
                {
                    sb.append(String.format("%02x", i & 0xff));
                }
    }
    
}
